// SPDX-Filename: ./edit_source.js
// SPDX-FileCopyrightText: 2021-2022 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
//
/* globals Choices */

import * as mµ	from '/js/mp_utils.js'

mµ.set_theme() // exceptional priority : full screen background to flash (FFF/000)

import * as sµ	from '/js/source_utils.js'
import * as xµ	from '/js/webext_utils.js'
import * as _		from '/js/js_utils.js'
import * as µ		from '/js/BOM_utils.js'
import * as DOMµ from '/js/DOM_parsererror_utils.js'
import * as g		from '/js/gettext_html_auto.js/gettext_html_auto.js'
import * as rj	from '/js/deps/renderjson.min.js'

const QUERYSTRING = new URL(window.location).searchParams
const form_btn_classes = ['btn', 'a', 'form_selection_input_btn']
let mp_i18n, source_objs
let default_val = {
	key: '',
	tags: {
		name: ['', false],
		lang: ['', false],
		country: ['', false],
		tz: ['', false],
		themes: [[], false],
		tech: [[], false],
		src_type: [[], false],
		res_type: [[], false]
	},
	src_url: ['', false],
	news_rss_url: ['', false],
	favicon_url: ['', false],
	search_url: ['', false],
	search_terms: [[''], false],
	search_url_web: ['', false],
	body: ['', false],
	results: ['', false],
	r_h1: ['', false, null, null, null],
	r_url: ['', false, null, null, null],
	r_dt: ['', false, [], null, null],
	r_txt: ['', false, null, null, null],
	r_img: ['', false, null, null, null],
	r_img_src: ['', true, null, null, null],
	r_img_alt: ['', true, null, null, null],
	r_by: ['', false, null, null, null],
	res_nb: ['', false, null, null, null],
}
async function load_existing_src (src_key) {
	let key = src_key
	let src = source_objs[key]
	let i
	if(_.is(src)) {
		default_val.key = key
		for(let k of Object.keys(src)) {
			if(k !== 'tags') {
				if(src[k]){
					if(default_val[k]){
						default_val[k][0] = src[k]
						default_val[k][1] = true
					}
					if(k.includes('_re')){
						i = k.split('_re')[0]
						default_val[i][2] = src[k]
					}
					if(k.includes('_attr')){
						i = k.split('_attr')[0]
						default_val[i][3] = src[k]
					}
					if(k.includes('_xpath')){
						i = k.split('_xpath')[0]
						default_val[i][4] = src[k]
					}
					if(k.includes('_fmt')){
						i = k.split('_fmt')[0]
						default_val[i][2] = default_val[i][2].push(src[k])
					}
				}
			} else
				for(let k_tags of Object.keys(src[k]))
					if(src[k][k_tags])
						if(default_val[k][k_tags]) {
							if(!Array.isArray(src[k][k_tags]) &&
								Array.isArray(default_val[k][k_tags][0]))
								default_val[k][k_tags][0] = [src[k][k_tags]]
							else
								default_val[k][k_tags][0] = src[k][k_tags]
							default_val[k][k_tags][1] = true
						}
		}
		if(Array.isArray(default_val.search_terms))
			default_val.search_terms[0] = default_val.search_terms[0][0]
		if(default_val.search_url[0].includes('{}')){
			let term = default_val.search_terms[0].replace(new RegExp(' ', 'g'),'+')
			default_val.search_url[0] = default_val.search_url[0].replace(
				'{}', term).replace(
				'{#}', 10).replace(
				'{+}', default_val.search_terms[0].replace(' ', '+'))
		}
	}
}
let default_fns_start = {
	completed: false,
	passed: false,
	// btn_next: QUERYSTRING.get('src') !== '',
	btn_next: 'part_start_form_btn_next',
	data: {
		tags_name: {type: 'required', test_passed: default_val.tags.name[1], value: ''},
		src_url: {type: 'required', test_passed: default_val.src_url[1], value: ''},
		news_rss: {type: 'optional', test_passed: default_val.news_rss_url[1], value: ''},
		favicon: {type: 'optional', test_passed: default_val.favicon_url[1], value: ''}
	}
}
let default_fns_timezone = {
	completed: false,
	nb_required: 3,
	nb_optional: 0,
	// btn_next: QUERYSTRING.get('src') !== ''
	btn_next: 'part_timezone_btn_next',
	data: {
		tags_lang: {type: 'required', test_passed: default_val.tags.lang[1], value: ''},
		tags_country: {type: 'required', test_passed: default_val.tags.country[1], value: ''},
		tags_tz: {type: 'required', test_passed: default_val.tags.tz[1], value: ''}
	},
	choices: {
		lang: null,
		country: null,
		tz: null
	}
}
let default_fns_selector = {
	completed: false,
	mode: 'get',
	doc: {
		sub_dom: null,
		body: null,
		results: null,
		r_h1: null,
		r_url: null,
		r_dt: null,
		res_nb: null,
		btn_next: QUERYSTRING.get('src') !== ''
	},
	text: {
		MSG_ERROR_VALUE_EMPTY: 'This value is required.',
		MSG_ERROR_BAD_SEARCH_TERM: 'Sorry but we didn\'t found the search terms in the URL. \
Please verify your search terms, else the source may use the POST method.',
		MSG_ERROR_IS_NOT_LINK: 'This element is not a link.',
		MSG_ERROR_IS_NOT_PATH: 'This value is not a CSS path.',
		MSG_ERROR_REGEX_NOT_VALID: 'This regex is not valid',
		MSG_ERROR_TEXT_CONTENT_EMPTY: 'Text not found. It might be in an attribute',
		MSG_ERROR_NOT_ELEMENT_FOUND: 'Sorry but not element found with this path.',
		MSG_ERROR_FAIL_TO_PARSE: 'This regex doesn\'t parse a text content of element.',
		MSG_ERROR_DATE_FOUND: 'We found a date we can\'t parse. Please provide its format.',
		MSG_TEXT_TITLE_BODY: 'Body',
		MSG_TEXT_TITLE_RESULTS: 'Result',
		MSG_TEXT_TITLE_R_H1: 'Title',
		MSG_TEXT_TITLE_R_URL: 'URL',
		MSG_TEXT_TITLE_R_DT: 'Date',
		MSG_TEXT_TITLE_R_TXT: 'Text',
		MSG_TEXT_TITLE_R_IMG: 'Image',
		MSG_TEXT_TITLE_R_IMG_SRC: 'Image Source',
		MSG_TEXT_TITLE_R_IMG_ALT: 'Image Alternative Text',
		MSG_TEXT_TITLE_R_BY: 'Author',
		MSG_TEXT_TITLE_RES_NB: 'Number of articles',
		MSG_TEXT_PLACEHOLDER_BODY: 'Choose a search bar',
		MSG_TEXT_PLACEHOLDER_RESULTS: 'Choose a results path',
		MSG_TEXT_PLACEHOLDER_R_H1: 'Choose a title path',
		MSG_TEXT_PLACEHOLDER_R_URL: 'Choose a link path',
		MSG_TEXT_PLACEHOLDER_R_DT: 'Choose a date path',
		MSG_TEXT_PLACEHOLDER_R_TXT: 'Choose a text path',
		MSG_TEXT_PLACEHOLDER_R_IMG: 'Choose a image path',
		MSG_TEXT_PLACEHOLDER_R_IMG_SRC: 'Choose a source image path',
		MSG_TEXT_PLACEHOLDER_R_IMG_ALT: 'Choose a alternative text image path',
		MSG_TEXT_PLACEHOLDER_R_BY: 'Choose a author path',
		MSG_TEXT_PLACEHOLDER_RES_NB: 'Choose a number of article path',
		MSG_TIPS_BODY: 'Tips: Click on the input when you see your search terms.',
		MSG_TIPS_RESULTS: 'Tips: Click on one result.',
		MSG_TIPS_R_H1: 'Tips: Click on title of the result.',
		MSG_TIPS_R_URL: 'Tips: Click on link of the result.',
		MSG_TIPS_R_DT: 'Tips: Click on date of the result.',
		MSG_TIPS_R_TXT: 'Tips: Click on text preview of the result.',
		MSG_TIPS_R_IMG: 'Tips: Click on image associate of the result.',
		MSG_TIPS_R_IMG_SRC: 'Tips: Click on image source associate of the result.',
		MSG_TIPS_R_IMG_ALT: 'Tips: Click on image alternative text associate of the result.',
		MSG_TIPS_R_BY: 'Tips: Click on author(s) of the result.',
		MSG_TIPS_RES_NB: 'Tips: Click on number of the result.',
		MSG_TEXT_INPUT_DISABLED: 'Pass',
		MSG_TEXT_BTN_CONFIRM: 'It is a good',
		MSG_TEXT_BTN_BACK: 'Change element',
		MSG_TEXT_BTN_SHOW: 'See element',
		MSG_TEXT_BTN_REG: 'Create/Use a regex',
		MSG_TEXT_BTN_ATTR: 'Choose a attribut',
		MSG_TEXT_BTN_NOT_DEFINE: 'Not defined for this JSON',
		MSG_TEXT_REG_PLACEHOLDER: 'Write your regex here…',
		MSG_TEXT_TOKEN_PLACEHOLDER: 'Write tokens here…',
	},
	data: {
		search_url: {type: 'required', test_passed: false, value: default_val.search_url[0],
			hidden_value: ''},
		search_terms: {type: 'required', test_passed: default_val.search_terms[1],
			value:default_val.search_terms[0]},
		search_url_web: {type: 'optional', test_passed: false,
			value: default_val.search_url_web[0]},
		body: {type: 'required', test_passed: false, value: default_val.body[0],
			css_class: 'form_selection_body', type_content: 'text', methode:['post']},
		results: {type: 'required', test_passed: false, value: default_val.results[0],
			css_class: 'form_selection_results', type_content: 'text',
			methode:['get', 'post', 'json']},
		r_h1: {type: 'required', test_passed: false, value: default_val.r_h1[0],
			css_class: 'form_selection_r_h1', type_content: 'text',
			type_regex:'string', regex: default_val.r_h1[2], attr: default_val.r_h1[3],
			nodes: ['h1', 'h2', 'h3', 'h4', 'h5'], methode:['get', 'post', 'json']},
		r_url: {type: 'required', test_passed: false, value: default_val.r_url[0],
			css_class: 'form_selection_r_url', type_content: 'link',
			type_regex:'string', regex: default_val.r_url[2], attr: default_val.r_url[3],
			nodes: ['a'], methode:['get', 'post', 'json']},
		r_dt: {type: 'required', test_passed: false, value: default_val.r_dt[0],
			css_class: 'form_selection_r_dt', type_content: 'text',
			type_regex:'list', regex: default_val.r_dt[2], index_regex: -1,
			attr: default_val.r_dt[3], nodes: ['time'], methode:['get', 'post', 'json']},
		r_txt: {type: 'optional', test_passed: false, is_passed: default_val.r_txt[0]==='',
			value: default_val.r_txt[0], css_class: 'form_selection_r_txt',
			type_content: 'text', type_regex:'string', regex: default_val.r_txt[2],
			attr: default_val.r_txt[3], nodes: ['p'], methode:['get', 'post', 'json']},
		r_img: {type: 'optional', test_passed: false, is_passed: default_val.r_img[0]==='',
			value: default_val.r_img[0], css_class: 'form_selection_r_img',
			type_content: 'image', type_regex:'string', regex: default_val.r_img[2],
			attr: default_val.r_img[3], nodes: ['img'], methode:['get', 'post', 'json']},
		r_img_src: {type: 'required', sub: 'r_img', test_passed: true, skip: true,
			value: default_val.r_img_src[0], css_class: 'form_selection_r_img',
			type_content: 'image', type_regex:'string', regex: default_val.r_img_src[2],
			attr: default_val.r_img_src[3], nodes: ['img'], methode:['get', 'post', 'json']},
		r_img_alt: {type: 'required', sub: 'r_img', test_passed: true, skip: true,
			value: default_val.r_img_alt[0], css_class: 'form_selection_r_img',
			type_content: 'image', type_regex:'string', regex: default_val.r_img_alt[2],
			attr: default_val.r_img_alt[3], nodes: ['img'], methode:['get', 'post', 'json']},
		//rajouter r_img_title
		r_by: {type: 'optional', test_passed: false, is_passed: default_val.r_by[0]==='',
			value: default_val.r_by[0], css_class: 'form_selection_r_by',
			type_content: 'text', type_regex:'string', regex: default_val.r_by[2],
			attr: default_val.r_by[3], nodes: [''], methode:['get', 'post', 'json']},
		res_nb: {type: 'optional', test_passed: false, is_passed: default_val.res_nb[0]==='',
			value: default_val.res_nb[0], css_class: 'form_selection_res_nb',
			type_content: 'text', type_regex:'string', regex: default_val.res_nb[2],
			attr: default_val.res_nb[3], nodes: [''], methode:['get', 'post', 'json']}
	},
	current_index: 0,
	old_index: null,
	form_input: [
		'body',
		'res_nb',
		'results',
		'r_h1',
		'r_url',
		'r_dt',
		'r_txt',
		'r_img',
		'r_img_src',
		'r_img_alt',
		'r_by'
	],
	class_list: [
		'form_selection_body',
		'form_selection_results',
		'form_selection_r_h1',
		'form_selection_r_url',
		'form_selection_r_dt',
		'form_selection_r_txt',
		'form_selection_r_img',
		'form_selection_r_by',
		'form_selection_res_nb'
	],
	sub_dom: {
		id_show_element: 'sub_dom_tmp_id',
		class_show_div: 'sub_dom_show_div',
		container_node: ['div', 'article']
	},
	request: {
		raw: null,
		content_type: null
	}
}
let default_fns_tags = {
	completed: false,
	time_fast: null,
	data: {
		themes: {type: 'required', test_passed: default_val.tags.themes[1],
			value: default_val.tags.themes[0]},
		tech: {type: 'required', test_passed: default_val.tags.tech[1],
			value: default_val.tags.tech[0]},
		src_type: {type: 'required', test_passed: default_val.tags.src_type[1],
			value: default_val.tags.src_type[0]},
		res_type: {type: 'required', test_passed: default_val.tags.res_type[1],
			value: default_val.tags.res_type[0]}
	},
	choice: {
		themes: null,
		src_type: null,
		res_type: null
	}
}
let fns_start, fns_timezone, fns_selector, fns_tags
let fns = {
	icons: {
		question: '&#63;',
		cross: '&#215;',
		check: '&#10003;',
		pen: '&#9998;'
	},
	test_integrity: null,
	request_raw: null,
	virtual_doc: null,
	src: {tags:{}},
	current_step: 1,
	percent: 0,
	load: false
}
function reset_fns(){
	// fns_start = _.deep_copy(default_fns_start)
	// fns_timezone = _.deep_copy(default_fns_timezone)
	// fns_selector = _.deep_copy(default_fns_selector)
	// fns_tags = _.deep_copy(default_fns_tags)
	fns_start = default_fns_start
	fns_timezone = default_fns_timezone
	fns_selector = default_fns_selector
	fns_tags = default_fns_tags
	fns.fns_parts = [fns_start, fns_timezone, fns_selector, fns_tags]
	part_selector_reset()
	/*if(timezone_choices!==null)
		fns_timezone.choices = timezone_choices
	if(tags_choices!==null)
		tags_choices = tags_choices*/
	form_display_gauge_progress_bar(0)
}
/**
 * Add a value to property in object.
 * @param {object} obj the object.
 * @param {string} key the name of property. It will be created if it doesn't exist
 * @param {*} val the value to add.
 */
function add_property(obj, key, val){
	obj[key] = val
}
/**
 * Remove a property from object.
 * @param {object} obj the object.
 * @param {string} key Property's name to be removed.
 */
function remove_property(obj, key){
	if( _.is(obj[key]))
		delete obj[key]
}
/**
 * Add CSS class to a node.
 * @param {string} id id of node in DOM.
 * @param {string} class_name classname you want to add.
 */
function form_addclass(id, class_name) {
	µ.id(id).classList.add(class_name)
}
/**
 * Remove CSS class from a node.
 * @param {string} id id of node in DOM.
 * @param {string} class_name classname you want to remove.
 */
function form_removeclass(id, class_name) {
	µ.id(id).classList.remove(class_name)
}
/**
 * Search a node by class name and remove this class for all
 * nodes found.
 * @param {string} class_name classname you want to remove.
 */
function form_search_and_removeclass(class_name) {
	let els = document.getElementsByClassName(class_name)
	for(let el of els)
		form_removeclass(el.id, class_name)
}
/**
 * Insert text element in a node.
 * @param {string} id id of node.
 * @param {string} msg text to be inserted in a node.
 */
function form_msg(id, msg) {
	µ.id(id).textContent = msg
}
/**
 * A toggle to show tips.
 * @param {HTMLElement} display node button of text tips.
 * @param {HTMLElement} tips text node.
 * @param {string} msg text of the tips.
 */
function form_tips(btn, tips) {
	let t = µ.id(tips)
	if(t.style.display === 'none' || t.style.display === '') {
		form_display(tips, true, 'inline-block')
		form_display(btn, false)
	} else {
		form_display(tips, false)
		form_display(btn, true, 'inline-block')
	}
}
/**
 * Show or hide node.
 * @param {string} id id of node.
 * @param {boolean} show true to display one node and false to make the same node undisplayed.
 * @param {string} type type of display (by default 'inline').
 */
function form_display(id, show, type='inline'){
	µ.id(id).style.display = show ? type : 'none'
}
/**
 * Add a value to an attribute in one node.
 * Also you have a possibility to trigger a event but it's optional.
 * @param {string} id id of node.
 * @param {*} value value to set.
 * @param {string} key attribute of node.
 * @param {string} event name of event. If it's not defined, no event is called.
 */
function form_set(id, value, key, evt=undefined) {
	try {
		let el = µ.id(id)
		el[key] = value
		el.dispatchEvent(new Event(evt))
	} catch(err) {
		console.error(err)
	}
}
/**
 * Process to check that all required inputs are valid for a part of the form.
 * @param {object} fns_part fns object link to part of the form.
 * @param {string | HTMLElement} btn_next id or node of button for the next part.
 */
function form_part_validator(fns_part, btn_next) {
	let is_valid = true
	// console.log('part_validator', fns_part)
	for(let d of Object.values(fns_part.data)) {
		if(d.type === 'required'){
			// console.log(d, d.test_passed)
			is_valid = is_valid && d.test_passed
		}
	}
	fns_part.completed = is_valid
	// form_display(_.is_str(btn_next) ? btn_next: btn_next.id, is_valid)
	let node_btn_next = _.is_str(btn_next) ? µ.id(btn_next) : btn_next
	if (is_valid)
		node_btn_next.click()
	// node_btn_next.click()
	form_update_progress_bar()
	return is_valid
}
/**
 * Get data input from a form part and return them.
 * @param {object} fns_part an object 'fns_' from form part.
 * @param {string} input name of the input form part.
 * @returns {object} return data object.
 */
function form_part_get_data(fns_part, input) {
	try {
		let data = fns_part.data[input]
		if(!_.is(data))
			throw new Error(`Data with key '${input}' doesn't exist.`)
		return data
	} catch(err) {
		console.error(err)
	}
}
/**
 * Start a process when an error is thrown for an input from the form.
 * @param {string} id_input id of node input.
 * @param {string} id_err id of node where you display text error.
 * @param {string} msg the text error.
 * @param {string} required If true, the class added to input is the class for required
 * input. Else, it's the class for warning input
 */
function form_input_err(id_input, id_err, msg, required=true) {
	form_addclass(id_input, required ? 'form_input_required': 'form_input_warning')
	form_msg(id_err, msg)
}
/**
 * Start a process for update a progress bar.
 */
function form_update_progress_bar() {
	fns.percent = form_calcul_gauge_progress_bar()
	form_display_gauge_progress_bar(fns.percent)
}
/**
 * Show a gauge in the HTML.
 * @param {number} gauge in percents for a gauge
 */
function form_display_gauge_progress_bar(gauge) {
	µ.id('progress_bar_gauge').style.width = gauge + '%'
	µ.id('progress_bar_text').textContent = gauge + '%'
}
/**
 * Calcul a gauge.
 * @returns {number} in percents for a gauge
 */
function form_calcul_gauge_progress_bar() {
	let gauge = 0
	for(let fns_part of Object.values(fns.fns_parts)) {
		for(let d of Object.values(fns_part.data)) {
			if(d.type === 'required')
				gauge += d.test_passed ? ((1 / fns.fns_parts.length) * 100) /
					form_count_required_input(fns_part): 0
		}
	}
	return Math.ceil(gauge)
}
/**
 * Count a number of required inputs in fns_part object.
 * It is used for establishing a gauge bar.
 * @see {@link form_calcul_gauge_progress_bar}
 * @param {object} fns_part a fns_part object for the form.
 * @returns {number} number of required inputs.
 */
function form_count_required_input(fns_part) {
	let count = 0
	for(let d of Object.values(fns_part.data)) {
		if(d.type === 'required')
			count += 1
	}
	return count
}
/**
 * Show or hide a part of the form.
 * @param {HTMLElement} fieldset a fieldset node.
 */
function form_toggle(fieldset) {
	fieldset.querySelector('.fld_ico').classList.toggle('fld_ico_hide')
	fieldset.classList.toggle('form_fieldset_hide')
}
/**
 * @description Build a template with parameters and return a object with some information
 * about the built template.
 *
 * The object returned contains this information.
 * @property {array} nodes list of nodes builds.
 * @property {number} nb_nodes number of nodes builds.
 * @property {HTMLElement} dom reference to default root div.
 * @property {array} params list of params you have to give in argument of this function.
 *
 * Node is created with parameters.
 * All nodes created have a root node div as a parent by default.
 * But you can change that with the 'parent' property.
 *
 * @example
 *			let template = [
 *					{node: 'div', id:'container_form_id' classList: ['container']},
 *							{node: 'p', id:'question_id', classList: ['question'], textContent:
 *								'It is a question', parent: 0},
 *							{node: 'div', id:'container_btn_id' classList: ['container'], parent: 0},
 *									{node: 'button', id:'btn_ok', classList: ['btn'], textContent: 'Ok !',
 *										parent: 2},
 *									{node: 'button', id:'btn_no', classList: ['btn'], textContent: 'No…',
 *										parent: 2}
 *			]
 *			let builder = form_builder(template)
 *			µ.id('insert_here').innerHTML = builder.dom.firstChild
 *
 * @description The node 'container_form_id' have a parent the root node div by default.
 * The nodes 'question_id' and 'container_btn_id' have a parent the node 'container_form_id'.
 * The nodes 'btn_ok' and 'btn_no' have a parent the node 'container_btn_id'.
 *
 * In HTML that give:
 * @example
 *			<div id='container_form_id' classList='container'>
 *					<p id='question_id'  classList='question'>It is a question</p>
 *					<div id='container_btn_id' classList='container'>
 *							<button id='btn_ok' classList='btn'>Ok !</button>
 *							<button id='btn_no' classList='btn'>No…</button>
 *					</div>
 *			</div>
 *
 * @param {array} params List of parameters for template build.
 * @param {array} params[].node The tag of node ('div', 'a', 'p', etc…). This property is
 * required.
 * @param {array} params[].parent Index of parent in the list params. Index start in 0. If it
 * is not defined the node have a root div as parent by default.
 * @param {array} params[].condition Condition boolean to created node.
 * @param {array} params[].all_other_HTML_attributs All others HTML attributes are supported.
 * @returns {object} object with some information about the template build.
 */
function form_builder(params) {
	let root = document.createElement('div')
	let black_list_key = ['node', 'parent', 'condition']
	let nodes = []
	for(let param of params) {
		if(typeof(param.node) === 'undefined')
			throw new Error('"node" parameter is not defined.')
		if(typeof(param.condition) !== 'undefined' && !param.condition)
			continue
		if(document.getElementById(param.id) !== null) {
			document.getElementById(param.id).remove()
		}
		let node = document.createElement(param.node)
		//console.log(param)
		for( let [k, v] of	Object.entries(param)){
			if(black_list_key.includes(k))
				continue
			try {
				if(k === 'classList')
					v.forEach( (cls) => { node.classList.add(cls) })
				else if(k === 'textContent')
					node.textContent = v
				else if(k === 'innerHTML')
					node.innerHTML = v
				else
					node.setAttribute(k, v)
			} catch(err) {
				console.error(err)
			}
		}
		nodes.push(node)
	}
	let index = 0
	for(let param of params) {
		if(typeof(param.condition) === 'undefined' || param.condition) {
			if(typeof(param.parent) === 'undefined') {
				root.appendChild(nodes[index])
			} else {
				let parent = nodes[param.parent]
				if(typeof(parent) === 'undefined')
					root.appendChild(nodes[index])
				else
					parent.appendChild(nodes[index])
			}
			index += 1
		}
	}
	let obj = {
		nodes: nodes,
		nb_nodes: nodes.length,
		dom: root,
		params: params
	}
	return obj
}

/**
 * Insert a node in the DOM.
 * @param {HTMLElement} el node to insert in DOM.
 * @param {HTMLElement} parent node parent.
 * @param {HTMLElement} ref child of parent to insert before the element. If it's null the
 * element is appended.
 */
function form_insert(el, parent, ref=null) {
	try {
		ref === null ? parent.appendChild(el): parent.insertBefore(el, ref)
	} catch (err) {
		console.error(err)
	}
}
/**
 * Remove a node in the DOM.
 * @param {string} id id of the node to remove.
 */
function form_remove(id) {
	try {
		let el = document.getElementById(id)
		if(el !== null)
			el.parentNode.removeChild(el)
	} catch (err) {
		console.error(err)
	}
}
/**
 *
 */
function part_start_err(name, msg, input, res_test, value) {
	form_input_err(`${name}_input`, `${name}_err`, msg, fns_start.data[input].required)
	part_start_form_update_data(input, res_test, value)
	form_part_validator(fns_start, fns_start.btn_next)
}
/**
 * Create a event for a start part of dynamic form.
 */
function part_start_form_event(source_keys) {
	sµ.load_src_reserved_names(source_objs)
	let part_start_form = µ.id('part_start_form_id')
	part_start_form.querySelector('legend').onclick = () => form_toggle(part_start_form)
	part_start_form.style.display = 'block'
	let html // the HTML content of the index of the source
	if(default_val.key !== '') {  // if we're editing a source rather than creating it
		form_part_validator(fns_start, fns_start.btn_next)
	}
	µ.id('name_input').value = ''
	µ.id('src_url_input').value = ''
	µ.id('news_rss_input').value = ''
	µ.id('favicon_url_input').value = ''
	µ.id('search_url_input').value = ''
	µ.id('search_url_web_input').value = ''
	µ.id('search_terms_input').value = ''
	µ.id('name_input').addEventListener('input', (evt) => {
		let val = _.triw(evt.target.value)
		let key = 'tags_name'
		if(val === '')
			return part_start_err('name', mp_i18n.gettext('Required'), key, false, val)
		if(val.length > 64)
			return part_start_err('name', mp_i18n.gettext('Too long (max 64)'), key, false, val)
		if(!sµ.is_src_name_uniq(val) && default_val.key === '')
			return part_start_err('name', mp_i18n.gettext('Already used'), key, false, val)
		form_removeclass('name_input', 'form_input_required')
		form_msg('name_err', '')
		add_property(fns.src.tags, 'name', val)
		part_start_form_update_data('tags_name', true, val)
		form_part_validator(fns_start, fns_start.btn_next)
	})
	µ.id('src_url_input').addEventListener('input', async (evt) => {
		µ.id('name_input').dispatchEvent(new Event('input'))
		let val = _.triw(evt.target.value)
		let key = 'src_url'
		if(val === '')
			return part_start_err('src_url', mp_i18n.gettext('Required'), key, false, val)
		if (!µ.is_valid_HTTP_URL(val))
			return part_start_err('src_url', mp_i18n.gettext('Invalid'), key, false, val)
		form_removeclass('src_url_input', 'form_input_required')
		form_msg('src_url_err', '')
		val = val.replace(/(.*)\/?/, '$1')
		if (source_keys.includes(val))	// Test if it's unique / unknown yet
			return part_start_err('src_url', mp_i18n.gettext('Already used'), key, 0, val)
		// Test if it's not in incompatible sources
		part_start_form_update_data('src_url', true, val)
		await mµ.request_permissions([`${val}/`])
		document.body.classList.add('waiting')
		html = await DOMµ.get_HTML_fragment(val)
		document.body.classList.remove('waiting')
		if (!html)
			return part_start_err('src_url', mp_i18n.gettext('Failed to load'), key, 0, val)
		form_removeclass('src_url_input', 'form_input_required')
		form_msg('src_url_err', '')
		form_part_validator(fns_start, fns_start.btn_next)
		let favicon = µ.get_favicon_URL(html, _.domain_part(val))
		if (favicon !== '')
			form_set('favicon_url_input', favicon, 'value', 'input')
		let news_rss = µ.get_RSS_URL(html, _.domain_part(val))
		if (news_rss !== '')
			form_set('news_rss_input', news_rss, 'value', 'input')
	})
	µ.id('news_rss_input').addEventListener('input', async (evt) => {
		let val = _.triw(evt.target.value)
		if(val === '') {
			form_msg('news_rss_err', '')
			form_removeclass('news_rss_input', 'form_input_required')
			part_start_form_update_data('news_rss', false, val)
			form_part_validator(fns_start, fns_start.btn_next)
			return
		}
		/*
		let key = 'news_rss'
		let res = sµ.test_attribute({}, fns.src, 'news_rss_url')
		if(res[1][0]) {
			return part_start_err('news_rss_url', mp_i18n.gettext('Incoherent'), key, 0,	val)
		}*/
		form_removeclass('news_rss_input', 'form_input_warning')
		form_msg('news_rss_err', '')
		add_property(fns.src, 'news_rss_url', val)
		part_start_form_update_data('news_rss', true, val)
	})
	µ.id('favicon_url_input').addEventListener('input', async (evt) => {
		let val = _.triw(evt.target.value)
		if(val === '') {
			form_display('favicon_url_img', false)
			form_msg('favicon_url_err', '')
			form_removeclass('favicon_url_input', 'form_input_required')
			part_start_form_update_data('favicon', false, val)
			form_part_validator(fns_start, fns_start.btn_next)
			return
		}
		/*
		let key = 'favicon'
		let res = sµ.test_attribute({}, fns.src, 'favicon_url')
		if(res[1][0]) {
			return part_start_err('favicon_url', mp_i18n.gettext('Incoherent'), key, 0,	val)
		}*/
		form_removeclass('favicon_url_input', 'form_input_warning')
		form_msg('favicon_url_err', '')
		µ.id('favicon_url_img').src = val
		form_display('favicon_url_img', true)
		add_property(fns.src, 'favicon_url', val)
		part_start_form_update_data('favicon', true, val)
	})
	µ.id('part_start_form_btn_next').onclick = () => {
		if(fns_start.completed && !fns_start.passed) {
			fns_start.passed = true
			form_toggle(part_start_form)
			part_timezone_init(html)
		}
	}
}
/**
 * Update data in fns_start.
 * @param {string} input the input name
 * @param {boolean} res_test the result of tests for the input
 * @param {*} value the input value
 */
function part_start_form_update_data(input, res_test, value) {
	fns_start.data[input].value = value
	fns_start.data[input].test_passed = res_test
}
/**
 * Init the timezone part of dynamic form.
 */
async function part_timezone_init(html) {
	let part_timezone = µ.id('part_timezone_id')
	part_timezone.style.display = 'block'
	fns_timezone.btn_next = µ.id('part_timezone_btn_next')
	part_timezone.querySelector('legend').onclick = () => form_toggle(part_timezone)
	part_timezone_create_lang_choices()
	await part_timezone_create_country_choices()
	part_timezone_create_timezone_choices()
	const elt_lg = fns_timezone.choices.lang.passedElement.element
	const elt_cn = fns_timezone.choices.country.passedElement.element
	const elt_tz = fns_timezone.choices.tz.passedElement.element
	elt_lg.addEventListener('addItem', (evt) => { event_timezone('lang', evt) })
	elt_cn.addEventListener('addItem', (evt) => { event_timezone('country', evt) })
	elt_tz.addEventListener('addItem', (evt) => { event_timezone('tz', evt) })
	let html_lang = '', html_country = ''
	if (html) {
		[html_lang, html_country] = µ.get_HTML_lang(html)
		console.log('[Meta-Press.es] html_lang, html_country', html_lang, html_country)
	}
	let selected_lang = default_val.key && default_val.tags.lang[0] || html_lang || ''
	fns_timezone.choices.lang.setChoiceByValue(selected_lang)
	let selected_country = default_val.key && default_val.tags.country[0] ||
		html_country && html_country.toLowerCase() ||
		html_lang !== 'en' && html_lang || ''
	fns_timezone.choices.country.setChoiceByValue(selected_country)
	let selected_tz = default_val.key && default_val.tags.tz[0] || ''
	fns_timezone.choices.tz.setChoiceByValue(selected_tz)
	form_set('check_all_tz', false, 'checked')
	if(default_val.key != '')
		form_part_validator(fns_timezone, fns_timezone.btn_next)
	µ.id('check_all_tz').onclick = (evt) => {
		part_timezone_update_timezone_choice(evt.target.checked ? '' :
			fns_timezone.choices.country.getValue().value)
	}
	µ.id('part_timezone_btn_next').onclick = () => {
		if(fns_timezone.completed) {
			form_toggle(µ.id('part_timezone_id'))
			part_selector_init()
		}
	}
}
/**
 * Create a choice list for source language with choice.js.
 */
let localisation_select_opt = {
	resetScrollPosition: false,
	duplicateItemsAllowed: false,
	searchResultLimit: 8,
	shouldSort: false,
	searchFields: ['label'],
	allowHTML: false,
	classNames: {
		containerInner: 'choices__inner form_required'
	}
}
let default_choice, LANG_NAME  // init by init()
function part_timezone_create_lang_choices() {
	let choices = [default_choice]
	for(let lang of µ.supported_ISO_639_2_language_locales())
		choices.push({label: `${LANG_NAME.of(lang)} (${lang})`, value: lang})
	fns_timezone.choices.lang = new Choices(µ.id('lang_select'), localisation_select_opt)
	fns_timezone.choices.lang.setChoices(choices, 'value', 'label', false)
}
async function part_timezone_create_country_choices() {
	let choices = [default_choice]
	const userLang = await mµ.get_wanted_locale()
	for(const [key, val] of Object.entries(µ.known_countries_and_code(userLang)))
		choices.push({label: `${val} (${key})`, value: key})
	fns_timezone.choices.country = new Choices(µ.id('country_select'), localisation_select_opt)
	fns_timezone.choices.country.setChoices(choices, 'value', 'label', false)
}
function part_timezone_create_timezone_choices() {
	let choices = [default_choice]
	if (Intl.supportedValuesOf)
		for(const val of Intl.supportedValuesOf('timeZone'))
			choices.push({label: val, value: val})
	fns_timezone.choices.tz = new Choices(µ.id('tz_select'), localisation_select_opt)
	fns_timezone.choices.tz.setChoices(choices, 'value', 'label', false)
}
function part_timezone_err(err, msg_err, el) {
	el.classList.add('form_input_required') // Choices.js elts miss id, so no form_addClass
	form_msg(err, msg_err)
	form_part_validator(fns_timezone, fns_timezone.btn_next)
}
function event_timezone(input_name, evt) {
	let val = evt.detail.value
	let el = fns_timezone.choices[input_name].containerInner.element
	fns_timezone.data[`tags_${input_name}`].value = val
	fns_timezone.data[`tags_${input_name}`].test_passed = val !== ''
	if(!fns_timezone.data[`tags_${input_name}`].test_passed)
		return part_timezone_err(`${input_name}_err`, mp_i18n.gettext('Required'), el)
	let obj = {tags: {name: fns.src.tags.name}}
	console.log('obj', obj)
	obj.tags[input_name] = val
	let res = sµ.test_attribute({}, obj, `tags.${input_name}`)
	console.log('res', res)
	if(!res[0])
		return part_timezone_err(`${input_name}_err`, mp_i18n.gettext('Incoherent'), el)
	fns_timezone.choices[input_name].containerInner.element.classList.remove(
		'form_input_required')
	form_msg(`${input_name}_err`, '')
	add_property(fns.src.tags, input_name, val)
	// console.log(fns)
	if(input_name === 'lang')
		if(!fns_timezone.choices.country.getValue() ||
			fns_timezone.choices.country.getValue().value === ''
		)
			fns_timezone.choices.country.setChoiceByValue(val)
	// else if(input_name === 'country')
	//	part_timezone_update_timezone_choice(val)  // once we had timezones by countries
	form_part_validator(fns_timezone, fns_timezone.btn_next)
}
/**
 * Set a timezone choice list with country code.
 * @param {number | string} country_code country code of country
 */
function part_timezone_update_timezone_choice(country_code) {
	let choices = [default_choice]
	for(let v of Object.values(country_code)){
		if(v instanceof Array)
			v = v[0]
		choices.push({label: v, value: v})
	}
	fns_timezone.choices.tz.clearStore()
	fns_timezone.choices.tz.setChoices(choices, 'value', 'label', false)
	fns_timezone.choices.tz.setChoiceByValue(
		choices.filter((el) => {return el.value !== ''})[0].value)
}
/**
 * Init the selector part of dynamic form.
 */
function part_selector_init() {
	/*µ.id('search_url_input').value = fns_selector.data.search_url.value
	µ.id('search_terms_input').value = fns_selector.data.search_terms.value
	µ.id('search_url_web_input').value = fns_selector.data.search_url_web.value*/
	// µ.id('HTTP_method').textContent = fns_selector.mode.toUpperCase()
	form_display('part_selector_id', true, 'block')
	part_selector_event()
}
const URL_SPACES = /\+|%20|%2B/g
function validation_part_selector(val, input_name) {
	if(val === '') {
		form_input_err(`${input_name}_input`, `${input_name}_err`, mp_i18n.gettext('Required'))
		fns_selector.data[input_name].test_passed = false
		return form_part_validator(fns_selector, 'finish_selection_part')
	}
	if(val.length < 4) {
		let a = input_name
		form_input_err(`${a}_input`, `${a}_err`, mp_i18n.gettext('Too short (min 3)'))
		fns_selector.data[input_name].test_passed = false
		return form_part_validator(fns_selector, 'finish_selection_part')
	}
	let clean_val = val.replace(URL_SPACES, ' ')
	fns_selector.data[input_name].value = clean_val
	if(input_name === 'search_terms') {
		let search_url = fns_selector.data.search_url.raw_value
		if (!search_url)
			return validation_part_selector('', 'search_url')
		if (!is_search_term(search_url, clean_val) && fns_selector.mode !== 'post') {
			form_input_err('search_terms_input', 'search_terms_err', mp_i18n.gettext('Not found'),0)
			fns_selector.data[input_name].test_passed = false
			/// form_display('method_post', true)
			/// µ.id('method_post').click()
			form_display('search_url_btn', false)
			return // form_part_validator(fns_selector, 'finish_selection_part') ?
		} else {
			form_display('method_post', false)
			/// form_display('search_url_btn', true)
			form_display('detect_rss', true)
			µ.id('detect_rss').click()
		}
	} else {
		fns_selector.data.search_url.raw_value = val
		if (!µ.is_valid_HTTP_URL(val)) {
			form_input_err(`${input_name}_input`, `${input_name}_err`, mp_i18n.gettext('Invalid'))
			fns_selector.data[input_name].test_passed = false
			return form_part_validator(fns_selector, 'finish_selection_part')
		}
	}
	form_removeclass(`${input_name}_input`, 'form_input_required')
	form_removeclass(`${input_name}_input`, 'form_input_warning')
	form_msg(`${input_name}_err`, '')
	fns_selector.data[input_name].test_passed = true
	form_part_validator(fns_selector, 'finish_selection_part')
}
function is_search_term(url, terms) {
	let u = new URL(url)
	let url_slices = []
	for (const s of u.pathname.split('/'))
		for (const r of s.split('='))  // because usinenouvelle.com puts search terms in plain URL
			if (r && r.length > 2)
				url_slices.push(r)
	for (const v of u.searchParams)
		if (v[1] && v[1].length > 2)
			url_slices.push(v[1])
	for (const s of url_slices)
		if (s.replace(URL_SPACES, ' ') === terms)  // terms already cleaned in validation_part_sel
			return true
	return false
}
/**
 * Create an event for a selector part of dynamic form.
 */
function part_selector_event() {
	let part_selector = µ.id('part_selector_id')
	part_selector.querySelector('legend').onclick = () => form_toggle(part_selector)
	µ.id('search_url_input').addEventListener('input', (evt) => {
		validation_part_selector(_.triw(evt.target.value), 'search_url')
	})
	µ.id('search_terms_input').addEventListener('input', (evt) => {
		validation_part_selector(_.triw(evt.target.value), 'search_terms')
	})
	µ.id('method_post').onclick = () => {
		part_selector_update_mode('post')
		µ.id('search_terms_input').dispatchEvent(new Event('input'))
		µ.id('search_url_btn').click()
	}
	µ.id('detect_rss').onclick = async () => {
		µ.id('detect_rss').disabled = true
		const url = new URL(µ.id('search_url_input').value)
		const terms = µ.id('search_terms_input').value
		if (!await mµ.request_one_host_perm(url.origin))
			return alert('/!\\')
		document.body.classList.add('waiting')
		for (let rss_path of sµ.SRC_DEF_TESTS.rss_url) {
			const rss_url = `${url.origin}${rss_path}`.replace('{}', terms)
			console.log('[Meta-Press.es] try detect RSS with', rss_url)
			const raw = await mµ.try_fetch(rss_url)
			if (raw.ok && raw.headers.get('content-type').includes('rss')) {
				console.log('[Meta-Press.es] found RSS', rss_url)
				if (!µ.id('search_url_web_id').value) {
					form_display('search_url_web_id', true)
					µ.id('search_url_web_input').value = url
					µ.id('search_url_web_input').dispatchEvent(new Event('input'))
				}
				µ.id('search_url_input').value = rss_url
				µ.id('search_url_input').dispatchEvent(new Event('input'))
				document.body.classList.remove('waiting')
				µ.id('detect_rss').disabled = false
				/// await _.delay(500)
				/// µ.id('mode_rss').click()
				const spaced_terms = terms.replace(URL_SPACES, ' ')
				fns.src.search_url = rss_url.replace(URL_SPACES, ' ').replace(spaced_terms, '{}')
				fns.src.search_url_web = µ.id('search_url_web_input').value.replace(
					URL_SPACES, ' ').replace(spaced_terms, '{}')
				part_selector_rss(raw)
				return
			} else {
				console.log('[Meta-Press.es] unsupported', rss_path, 'RSS path')
				if (raw.ok) console.log('got this content-type', raw.headers.get('content-type'))
				await _.delay(1000)
			}
		}
		console.log('[Meta-Press.es] finished, RSS not found')
		document.body.classList.remove('waiting')
		µ.id('detect_rss').disabled = false
		µ.id('detect_rss').classList.add('form_input_warning')
		µ.id('detect_rss').classList.add('form_input_error')
		form_msg('search_terms_err', 'RSS not found')
		µ.id('search_url_btn').click()
	}
	µ.id('search_url_btn').onclick = async () => {
		if(default_val.key){
			validation_part_selector(fns_selector.data.search_url.value, 'search_url')
			validation_part_selector(fns_selector.data.search_terms.value, 'search_terms')
		}
		if(fns_selector.data.search_url.test_passed &&
			fns_selector.data.search_terms.test_passed
		) {
			part_selector_reset()
			const spaced_terms = µ.id('search_terms_input').value.replace(URL_SPACES, ' ')
			const search_url = µ.id('search_url_input').value
			fns.src.search_url = search_url.replace(URL_SPACES, ' ').replace(spaced_terms, '{}')
			await part_selector_fetch(fns_selector.data.search_url.raw_value)
			if(!µ.id('mode_selection').style.display === 'block') {
				µ.id('mode_selection').style.display = 'block'
				µ.id('src_ctype').textContent = fns_selector.request.content_type
				µ.id('mode_css').onclick = (evt) => {
					form_search_and_removeclass('form_mode_selected')
					form_addclass(evt.target.id, 'form_mode_selected')
					remove_property(fns.src, 'search_url_web')
					form_display('search_url_web_id', false)
					part_selector_before_sub_dom()
				}
				µ.id('mode_json').onclick = (evt) => {
					form_search_and_removeclass('form_mode_selected')
					form_addclass(evt.target.id, 'form_mode_selected')
					form_display('search_url_web_id', true)
					part_selector_before_sub_dom()
				}
				µ.id('mode_rss').onclick = (evt) => {
					form_search_and_removeclass('form_mode_selected')
					form_addclass(evt.target.id, 'form_mode_selected')
					form_display('search_url_web_id', true)
					part_selector_rss()
				}
				form_addclass(`mode_${fns_selector.request.content_type}`, 'form_mode_selected')
			} else {
				await part_selector_fetch(fns_selector.data.search_url.raw_value)
				part_selector_before_sub_dom()
			}
		}
	}
	µ.id('finish_selection_part').onclick = () => {
		form_toggle(µ.id('part_selector_id'))
		part_tags_init()
	}
}
/**
 *	Add method to the new source.
 * @param {string} mode method for the source (GET | POST | JSON | RSS)
 */
function part_selector_update_mode(mode) {
	if (mode === 'get')
		remove_property(fns.src, 'method')
	else
		add_property(fns.src, 'method', mode.toUpperCase())
	fns_selector.mode = mode.toLowerCase()
}
async function part_selector_rss(raw, ) {
	if (!raw) {
		let url = new URL(µ.id('search_url_input').value)
		await mµ.request_one_host_perm(url.origin)
		document.body.classList.add('waiting')
		let raw = await mµ.try_fetch(url)
		document.body.classList.remove('waiting')
		if (!raw.ok) return form_input_err(
			'search_url_input', 'search_url_err', mp_i18n.gettext('Failed to load'), 0)
		if (!raw.headers.get('content-type').include('rss')) return form_input_err(
			'search_url_input', 'search_url_err', mp_i18n.gettext('Invalid'), 0)
	}
	let rep = mµ.DOM_parse_text_response(await raw.text(), raw.headers.get('content-type'))
	if (!rep) {
		form_input_err('search_url_input', 'search_url_err', mp_i18n.gettext('Incoherent'), 0)
		return
	}
	fns.src.type = 'XML'
	form_removeclass('search_url_input', 'form_input_required')
	form_removeclass('search_url_input', 'form_input_warning')
	form_msg('search_url_err', '')
	part_tags_init()
}
/**
 * Process to init all sub dom have need.
 * @async
 */
async function part_selector_before_sub_dom() {
	// form_remove('text_type_content_detected')
	form_display('sub_dom', false)
	part_selector_reset()
	let url = fns_selector.data.search_url.value
	let headline = fns_start.data.headline.value
	await part_selector_fetch(fns_selector.mode === 'post' ? headline : url)
	if(!await part_selector_try_get_virtual_doc()) {
		let template = [
			{node: 'p', id: 'request_error', textContent: 'This method trigger an error. \
				Please try other method',
			style:'color: red; margin:auto;'}
		]
		let builder = form_builder(template)
		form_insert(builder.dom.firstChild, µ.id('part_selector_id'), µ.id('sub_dom'))
		return
	} else {
		form_remove('request_error')
	}
	if(fns_selector.mode === 'get' ||  fns_selector.mode === 'post') {
		let body = fns.virtual_doc.body
		body = new DOMParser().parseFromString( body.innerHTML, 'text/html')
		body = body.children[0].children[1]
		part_selector_depth_clean_virtual_doc(body.childNodes)
		part_selector_depth_clean_virtual_doc(body.childNodes)
		part_selector_child_have_text_content(body.childNodes)
		part_selector_set_sub_dom(body)
	} else if(fns_selector.mode === 'json') {
		let search_url_web = µ.dry_triw(µ.id('search_url_web_input').value)
		if(search_url_web === '') {
			add_property(fns.src, 'search_url_web',
				fns_start.data.headline.value)
			form_set('search_url_web_input', fns_start.data.headline.value, 'value')
		} else {
			add_property(fns.src, 'search_url_web', search_url_web)
		}
		let json = fns.virtual_doc
		part_selector_set_sub_dom(json)
	}
	form_display('sub_dom', true, 'block')
	fns_selector.current_index = 0
	let test_terms = part_selector_build_list_test_terms()
	is_fast(fns_selector.data.search_url.hidden_value, test_terms).then(
		(response) => {
			console.log(response)
			fns_tags.time_fast = response
		}
	)
	part_selector_build_input()
}
/**
 * Reset the selector part from the dynamic form.
 */
function part_selector_reset() {
	fns_selector.current_index = 0
	fns_selector.old_index = null
	fns_selector.completed = false
	for(let input of fns_selector.form_input) {
		if(µ.id(`${input}_id`)) {
			let child = µ.id(`${input}_id`)
			form_remove(child.id)
			let data = fns_selector.data[input]
			if(input === 'r_dt')
				for(let count = 1; count <= data.regex.length + 1; count++)
					remove_property(fns.src, `${input}_fmt_${count}`)
			part_selector_reset_data(input)
			remove_property(fns.src, input)
			remove_property(fns.src, `${input}_re`)
			remove_property(fns.src, `${input}_attr`)
			remove_property(fns.src, `${input}_xpath`)
		}
	}
}
/**
 * Reset the data to selector part from the dynamic part.
 * @param {string} input the name of input.
 */
function part_selector_reset_data(input) {
	let data = fns_selector.data[input]
	data.value = ''
	if(typeof(data.regex) !== 'undefined')
		data.type_regex === 'string' ? data.regex = null: data.regex = []
	if(typeof(data.attr) !== 'undefined')
		data.attr = null
	if (typeof(data.sub) !== 'undefined') {
		data.test_passed = true
		data.skip = true
	} else {
		data.test_passed = false
	}
}
/**
 * Perform a request on the specified url.
 * @async
 * @param {string} url url to perform a request.
 * @param {string} method method for the request (GET | POST). GET by default.
 * @param {string} body body of the request. null by default.
 */
async function part_selector_fetch(url, method='GET', body=null) {
	await mµ.request_one_host_perm(url)
	const raw = await mµ.try_fetch(url, method, body)
	fns_selector.request.raw = raw
	fns_selector.request.content_type = raw.headers.get('Content-Type').
		split('/')[1].
		split('+')[0]
}
/**
 * Create a body with a data from form and return it.
 * @async
 * @param {FormData} data FormData object from the form.
 * @returns {string} build body for the request.
 */
async function part_selector_build_post_request(data) {
	let body = ''
	for(let [key, value] of data.entries())
		body === '' ? body += `${key}=${value}`: body += `&${key}=${value}`
	return body
}
/**
 * Try to get a virtual document with the request.
 * @async
 * @see {@link part_selector_fetch}
 * @returns {boolean} true or false to indicate if it's a success or not.
 */
async function part_selector_try_get_virtual_doc() {
	if(fns_selector.mode === 'get' ||  fns_selector.mode === 'post') {
		try {
			let raw_text = await fns_selector.request.raw.text()
			fns.virtual_doc = new DOMParser().parseFromString(raw_text, 'text/html')
		} catch(err) {
			return false
		}
	} else if (fns_selector.mode === 'json') {
		try {
			let raw_json = await fns_selector.request.raw.json()
			fns.virtual_doc = raw_json
		} catch(err) {
			return false
		}
	}
	return true
}
/**
 * Clean the virtual document.
 * @param {NodeList} childs List of node of body from virtual document.
 */
function part_selector_depth_clean_virtual_doc(childs){
	let tags_to_drop = ['meta', 'link', 'svg', 'style', 'script', 'header', 'footer', 'iframe',
		'embed', 'object']
	let reg = new RegExp(/on.*/)
	for(let c of childs) {
		if(tags_to_drop.includes(c.localName)) {
			console.log(c)
			c.parentNode.removeChild(c)
		} else if(c.childNodes.length > 0)
			part_selector_depth_clean_virtual_doc(c.childNodes)
		if(c.localName === 'a') {
			c.disabled = true
			// c.removeAttribute('href') // remove href pose problème avec
			// JSON/form, car _attr est href de base dans la source mais une fois
			// remove le formulaire ne le retrouve pas et plante
			// -> à voir si ca pose problème avec le formulaire de base
		}
		/**
		else if(c.localName === 'script'){
				c.removeAttribute('src')
				c.textContent = ''
		}*/ else if(c.localName === 'input' || c.localName === 'button'){
			c.removeAttribute('type')
			if(c.localName === 'button')
				c.disabled = true
		}
		if(typeof(c.attributes) !== 'undefined')
			for(let attr of c.attributes)
				if(attr.localName.match(reg) !== [])
					c.removeAttribute(attr)
	}
}
/**
 * Add a class to parent div of given node to show it in the sub dom.
 * @param {HTMLElement} node a node.
 */
function part_selector_add_class_in_first_container(node, frame_trigger) {
	/**
			while(node.localName !== 'body') {
					node = node.parentNode
					if(node.children.length > 9)
							frame_trigger = true
					if(fns_selector.sub_dom.container_node.includes(node.localName)
							&& node.childNodes.length > 1
						//&& !node.classList.contains(fns_selector.sub_dom.class_show_div)
							&& frame_trigger
					) {
							node.classList.add(fns_selector.sub_dom.class_show_div)
							break
					}
			} */
	node = node.parentNode
	if(node.children.length > 4)
		frame_trigger = true
	if(fns_selector.sub_dom.container_node.includes(node.localName)
		&& node.childNodes.length > 1
		//&& !node.classList.contains(fns_selector.sub_dom.class_show_div)
		&& frame_trigger
	) {
		node.classList.add(fns_selector.sub_dom.class_show_div)
	}
	return frame_trigger
}
/**
 * Process to know whose child have text content.
 * @param {NodeList} childs List of node.
 */
function part_selector_child_have_text_content(childs) {
	let count = 0
	let frame_trigger = true
	while(count < childs.length) {
		let child = childs[count]
		if(child.nodeName === '#comment' || child.nodeName === '#text') {
			count += 1
			continue
		}
		if(µ.dry_triw(child.textContent) !== ''
			&& !fns_selector.sub_dom.container_node.includes(child.localName)
		)
			frame_trigger = part_selector_add_class_in_first_container(child, frame_trigger)
		if(child.childNodes.length > 0)
			childs = [...childs, ...child.childNodes]
		count += 1
	}
}
/**
 * Try to determine if a source is fast to respond from the specified url.
 * @async
 * @param {string} url url of source.
 * @param {array} terms list of terms for testing the source.
 * @returns {number} the responses average time.
 */
async function is_fast(url, terms) {
	let times = []
	for(let term of terms) {
		let testUrl = url.replace('{}', term)
		let start = new Date()
		await part_selector_fetch(testUrl, fns_selector.mode)
		let end = new Date()
		times.push(end - start)
	}
	let average = times.reduce( (preVal, nextVal) => { return preVal + nextVal}) / times.length
	return (average / 1000).toFixed(3)
}
/**
 * Build a list of most positive terms search used from file source.json
 * @returns {array} Lists of terms
 */
function part_selector_build_list_test_terms() {
	let terms = []
	for(let val of Object.values(source_objs)) {
		if(_.is(val.search_terms)) {
			for(let positive_term of val.search_terms) {
				if(positive_term === '')
					continue
				let exist = terms.filter( (t) => {return t.name === positive_term})
				if(exist.length !== 0) {
					exist[0].count += 1
				} else {
					let new_term = {name: positive_term, count: 1}
					terms.push(new_term)
				}
			}
		}
	}
	return terms.sort( (a, b) => {
		if(a.count > b.count)
			return -1
		else if(a.count < b.count)
			return 1
		return 0
	}).slice(0, 5).map((val) => {return val.name})
}
/**
 * Process to build a input for selector sub dom in selector part from dynamic form.
 */
function part_selector_build_input() {
	fns_selector.old_index = null
	if(fns_selector.current_index < fns_selector.form_input.length) {
		let input = fns_selector.form_input[fns_selector.current_index]
		let data = form_part_get_data(fns_selector, input)
		if(!data.methode.includes(fns_selector.mode)) {
			data.test_passed = true
			fns_selector.current_index += 1
			part_selector_build_input()
			return
		}
		if(typeof(data.skip) !== 'undefined' && data.skip) {
			fns_selector.current_index += 1
			part_selector_build_input()
			return
		}
		let template	= null
		if(data.type === 'required') {
			template = [
				{node: 'div', id:`${input}_id`, name:input,
					classList: ['form_selection_container'], style:'display: none'},
				{node:'div', classList: ['input_container'], parent:0},
				{node: 'label', id:`${input}_text`, parent:1},
				{node: 'span', style:'font-weight: bold;',
					textContent:fns_selector.text[`MSG_TEXT_TITLE_${input.toUpperCase()}`],
					classList: [`${data.css_class}_text`], parent: 2},
				{node: 'text', textContent:': ', parent: 2},
				{node: 'span', id:`${input}_final`, classList: ['form_selection_text'],
					parent: 2},
				{node: 'input', id: `${input}_input`,
					placeholder:fns_selector.text[`MSG_TEXT_PLACEHOLDER_${input.toUpperCase()}`],
					title:'Ex: https://www.custom-source.eu', type:'text',
					classList: ['form_input_text'], value: data.value, parent:1},
				{node:'button', id:`${input}_btn_confirm`, classList: form_btn_classes,
					textContent: fns_selector.text.MSG_TEXT_BTN_CONFIRM, parent:1},
				{node:'button', id:`${input}_btn_back`, classList: form_btn_classes,
					style: 'display: none;', textContent:fns_selector.text.MSG_TEXT_BTN_BACK, parent:1},
				{node:'button', id:`${input}_btn_show`, classList: form_btn_classes,
					textContent: fns_selector.text.MSG_TEXT_BTN_SHOW, parent:1},
				{node:'button', id:`${input}_btn_not_define`, classList: form_btn_classes,
					textContent: fns_selector.text.MSG_TEXT_BTN_NOT_DEFINE,
					condition: fns_selector.mode === 'json' && input === 'results', parent:1},
				{node:'button', id:`${input}_btn_attr`, classList: form_btn_classes,
					style: 'display: none;', textContent:fns_selector.text.MSG_TEXT_BTN_ATTR, parent:1},
				{node: 'span', id:`${input}_final_attr`, classList: ['form_selection_text'],
					parent: 1},
				{node:'button', id:`${input}_btn_reg`, classList: form_btn_classes,
					style: 'display: none;', textContent: fns_selector.text.MSG_TEXT_BTN_REG, parent:1},
				{node: 'span', id:`${input}_final_reg`, classList: ['form_selection_text'],
					parent: 1},
				{node:'p', id:`${input}_tips`, classList: ['form_input_tips'], parent:1},
				{node:'button', id:`${input}_display_tips`, classList: form_btn_classes, parent:1},
				{node:'p', id:`${input}_err`, classList: ['form_input_error'], parent:0}
			]
		} else {
			template = [
				{node: 'div', id:`${input}_id`, name:input,
					classList: ['form_selection_container'], style:'display: none'},
				{node:'div', classList: ['input_container'], parent:0},
				{node: 'label', id:`${input}_text`, parent:1},
				{node: 'span', style:'font-weight: bold;',
					textContent:fns_selector.text[`MSG_TEXT_TITLE_${input.toUpperCase()}`],
					classList: [`${data.css_class}_text`], parent: 2},
				{node: 'text', textContent:': ', parent: 2},
				{node: 'span', id:`${input}_final`, classList: ['form_selection_text'],
					parent: 2},
				{node: 'input', id: `${input}_input`,
					placeholder:fns_selector.text[`MSG_TEXT_PLACEHOLDER_${input.toUpperCase()}`],
					title:'Ex: https://www.custom-source.eu', type:'text',
					classList: ['form_input_text'], value: data.value, parent:1},
				{node:'button', id:`${input}_btn_confirm`, classList: form_btn_classes,
					textContent: fns_selector.text.MSG_TEXT_BTN_CONFIRM, parent:1},
				{node:'button', id:`${input}_btn_back`, classList: form_btn_classes,
					style: 'display: none;', textContent:fns_selector.text.MSG_TEXT_BTN_BACK, parent:1},
				{node:'button', id:`${input}_btn_show`, classList: form_btn_classes,
					textContent: fns_selector.text.MSG_TEXT_BTN_SHOW, parent:1},
				{node:'button', id:`${input}_btn_attr`, classList: form_btn_classes,
					style: 'display: none;', textContent:fns_selector.text.MSG_TEXT_BTN_ATTR, parent:1},
				{node: 'span', id:`${input}_final_attr`, classList: ['form_selection_text'],
					parent: 1},
				{node:'button', id:`${input}_btn_reg`, classList: form_btn_classes,
					style: 'display: none;', textContent: fns_selector.text.MSG_TEXT_BTN_REG, parent:1},
				{node: 'span', id:`${input}_final_reg`, classList: ['form_selection_text'],
					parent: 1},
				{node:'button', id:`${input}_btn_disable`, classList: form_btn_classes,
					innerHTML: fns.icons.cross, parent:1},
				{node:'p', id:`${input}_tips`, classList: ['form_input_tips'], parent:1},
				{node:'button', id:`${input}_display_tips`, classList: form_btn_classes, parent:1},
				{node:'p', id:`${input}_err`, classList: ['form_input_error'], parent:0}
			]
		}
		let builder = form_builder(template)
		let child = builder.dom.firstChild
		let sub_dom = µ.id('sub_dom')
		form_insert(child, µ.id('part_selectord_id'), sub_dom)
		if(data.type === 'optional') {
			µ.id(`${input}_btn_disable`).onclick = (evt) => {
				let comp = document.createElement('div')
				comp.innerHTML = fns_selector.text.MSG_TEXT_BTN_BACK
				part_selector_destroy_regex(input)
				part_selector_destroy_attr(input)
				if(evt.target.innerHTML !== comp.innerHTML) {
					evt.target.innerHTML = fns_selector.text.MSG_TEXT_BTN_BACK
					form_display(`${input}_input`, false)
					form_display(`${input}_btn_confirm`, false)
					form_display(`${input}_btn_show`, false)
					form_display(`${input}_btn_back`, false)
					form_display(`${input}_btn_reg`, false)
					form_display(`${input}_btn_attr`, false)
					form_msg(`${input}_final`, fns_selector.text.MSG_TEXT_INPUT_DISABLED)
					data.test_passed = false
					data.is_passed = true
					remove_property(fns.src, input)
					if(fns_selector.old_index !== null) {
						part_selector_input_focus()
					} else {
						form_removeclass(`${input}_input`, 'form_input_required')
						form_msg(`${input}_err`, '')
						data.test_passed = false
						part_selector_clear_selection()
						part_selector_build_input()
						form_part_validator(fns_selector, 'finish_selection_part')
					}
				} else {
					evt.target.innerHTML = fns.icons.cross
					form_display(`${input}_input`, true)
					if(data.test_passed) {
						form_display(`${input}_btn_back`, true)
						form_display(`${input}_btn_reg`, true)
					} else {
						form_display(`${input}_btn_confirm`, true)
					}
					form_display(`${input}_btn_show`, true)
					form_msg(`${input}_final`, '')
					data.test_passed = false
					data.is_passed = default_val[input][0] === ''
					part_selector_input_focus()
				}
			}
		}
		if(default_val.key != '' && data.is_passed){
			fns_selector.current_index += 1
			µ.id(`${input}_btn_disable`).click()
		}
		fns_selector.doc[input] = µ.id(`${input}_id`)
		//let doc = fns_selector.doc[input]
		if(data.type === 'optional')
			form_addclass(`${input}_input`, 'form_warning')
		if(typeof(data.sub) !== 'undefined') {
			form_addclass(`${input}_id`, 'form_selection_container_sub')
		}
		if(data.value !== '') {
			let el = part_selector_get_sub_dom_element(data.value)[0]
			if(typeof(el) !== 'undefined') {
				el.classList.add(data.css_class)
			}
		}
		let re = '^.*(#[^#]*)$'
		let value = µ.regextract(re, data.value)
		form_set(`${input}_input`, value, 'value')
		form_msg(`${input}_display_tips`, fns.icons.question)
		part_selector_new_sub_dom_listener(input, data)
		if(fns_selector.mode === 'json') {
			let old_input = fns_selector.form_input[fns_selector.current_index - 1]
			if(typeof(old_input) !== 'undefined')  {
				let old_data = form_part_get_data(fns_selector, old_input)
				if(old_data.value !== '' && old_data.value !== '""')
					part_selector_json_show(old_data.value)
			}
		}
		µ.id(`${input}_display_tips`).onclick = () => {
			let display = µ.id(`${input}_display_tips`)
			let tips = µ.id(`${input}_tips`)
			form_tips(display, tips, fns_selector.text[`MSG_TIPS_${input.toUpperCase()}`] )
		}
		µ.id(`${input}_input`).addEventListener('input', (evt) => {
			part_selector_destroy_attr(input)
			part_selector_destroy_regex(input)
			form_search_and_removeclass('form_input_required')
			form_msg(`${input}_err`, '')
			data.value = evt.target.value
		})
		µ.id(`${input}_btn_back`).onclick = () => {
			part_selector_clear_selection()
			form_removeclass(`${input}_btn_reg`, 'form_input_required')
			form_msg(`${input}_err`, '')
			part_selector_reset_data(input)
			part_selector_input_focus()
			form_msg(`${input}_final`, '')
			//form_set(`${input}_final`, '', 'textContent')
			form_display(`${input}_input`, true)
			form_display(`${input}_btn_confirm`, true)
			form_display(`${input}_btn_back`, false)
			fns_selector.mode === 'json' && input === 'results'
				? form_display(`${input}_btn_not_define`, true): null
			form_display(`${input}_btn_attr`, false)
			form_display(`${input}_btn_reg`, false)
			form_part_validator(fns_selector, 'finish_selection_part')
		}
		µ.id(`${input}_btn_show`).onclick = () => {
			part_selector_clear_selection()
			let path = _.triw(data.value)
			if(path === '') {
				form_input_err(`${input}_input`, `${input}_err`,
					fns_selector.text.MSG_ERROR_VALUE_EMPTY)
				data.test_passed = false
				form_part_validator(fns_selector, 'finish_selection_part')
				return
			}
			if(fns_selector.mode === 'get' || fns_selector.mode === 'post') {
				let el = µ.id('sub_dom').querySelector(path)
				if(el.id === '')
					el.setAttribute('id', fns_selector.sub_dom.id_show_element)
				el.classList.add(data.css_class)
				window.location.hash = el.id
				el.id ===  fns_selector.sub_dom.id_show_element ? el.removeAttribute('id'): null
			} else if(fns_selector.mode === 'json') {
				let el = part_selector_json_show(path)
				el = el.parentNode
				let keys = el.querySelectorAll('.key')
				path = path.split('.').reverse()[0]
				for(let key of keys) {
					if(key.textContent === `'${path}'`) {
						key.classList.add(data.css_class)
						break
					}
				}
			}
			form_removeclass(`${input}_input`, 'form_input_required')
			form_msg(`${input}_err`, '')
			data.test_passed = true
		}
		if(fns_selector.mode === 'json' && input === 'results') {
			µ.id(`${input}_btn_not_define`).addEventListener( 'click', () => {
				data.value = '""'
				data.test_passed = true
				form_removeclass(`${input}_input`, 'form_input_required')
				form_msg(`${input}_err`, '')
				form_display(`${input}_input`, false)
				form_display(`${input}_btn_confirm`, false)
				form_display(`${input}_btn_back`, true)
				fns_selector.mode === 'json' && input === 'results'
					? form_display(`${input}_btn_not_define`, false): null
				_.is(data.attr) && fns_selector.mode !== 'json'
					? form_display(`${input}_btn_attr`, true): null
				_.is(data.attr) ? form_display(`${input}_btn_reg`, true): null
				form_msg(`${input}_final`, 'Results is not defined')
				if(fns_selector.old_index === null
					&& form_part_get_data(fns_selector,
						fns_selector.form_input[fns_selector.current_index - 1]).test_passed
				)
					part_selector_build_input()
				else
					part_selector_input_focus()
				add_property(fns.src, input, data.value)
				form_part_validator(fns_selector, 'finish_selection_part')
			})
		}
		if(_.is(data.regex)) {
			µ.id(`${input}_btn_reg`).onclick = () => {
				let val = part_selector_sub_dom_get_element_text(input).value
				if(val === '' || val === null) {
					part_selector_destroy_regex(input)
					form_input_err(`${input}_btn_reg`,`${input}_err`,
						fns_selector.text.MSG_ERROR_TEXT_CONTENT_EMPTY)
					return
				}
				form_removeclass(`${input}_btn_reg`, 'form_input_required')
				form_msg(`${input}_err`, '')
				if(!µ.id(`${input}_reg`))
					part_selector_init_regex(input, data, val)
				else
					part_selector_destroy_regex(input)
			}
		}
		if(typeof(data.attr) !== 'undefined' && fns_selector.mode !== 'json') {
			µ.id(`${input}_btn_attr`).onclick = () => {
				form_removeclass(`${input}_btn_reg`, 'form_input_required')
				form_msg(`${input}_err`, '')
				if(!µ.id(`${input}_attr`)) {
					let contents = part_selector_get_content(data)
					if(contents !== [])
						part_selector_init_attr(contents, input)
				} else {
					part_selector_destroy_attr(input)
				}
			}
		}
		µ.id(`${input}_btn_confirm`).onclick = () => {
			form_removeclass(`${input}_input`, 'form_input_required')
			form_msg(`${input}_err`, '')
			let val = µ.id(`${input}_input`).value
			if(val === '') {
				form_input_err(`${input}_input`, `${input}_err`,
					fns_selector.text.MSG_ERROR_VALUE_EMPTY)
				data.test_passed = false
				form_part_validator(fns_selector, 'finish_selection_part')
				return
			}
			if(data.type_content === 'link' && fns_selector.mode !== 'json') {
				let el = µ.id('sub_dom').querySelector(val)
				if(el.localName !== 'a') {
					form_input_err(`${input}_input`, `${input}_err`,
						fns_selector.text.MSG_ERROR_IS_NOT_LINK)
					data.test_passed = false
					form_part_validator(fns_selector, 'finish_selection_part')
					return
				}
			}
			let el
			if(fns_selector.mode === 'get' ||  fns_selector.mode === 'post')
				el = µ.id('sub_dom').querySelector(val)
			if(fns_selector.mode === 'json') {
				el = fns.virtual_doc
				let path = val.split('.')
				for(let p of path) {
					if(el.length === undefined)
						el = el[p]
					else
						el = el[0][p]
					if(typeof(el) === 'undefined')
						break
				}
			}
			if(el === null	|| typeof(el) === 'undefined') {
				form_input_err(`${input}_input`, `${input}_err`,
					fns_selector.text.MSG_ERROR_NOT_ELEMENT_FOUND)
				data.test_passed = false
				form_part_validator(fns_selector, 'finish_selection_part')
				return
			}
			data.value = val
			if(_.is(data.attr) && input !== 'r_img' && fns_selector.mode !== 'json') {
				let text = part_selector_sub_dom_get_element_text(input).value
				if(text === '' || text === null) {
					let contents = part_selector_get_content(data)
					if(contents !== [])
						part_selector_init_attr(contents, input)
					form_input_err(`${input}_btn_attr`, `${input}_err`,
						fns_selector.text.MSG_ERROR_TEXT_CONTENT_EMPTY)
					data.test_passed = false
					form_part_validator(fns_selector, 'finish_selection_part')
					return
				}
				form_removeclass(`${input}_btn_attr`, 'form_input_required')
				part_selector_destroy_attr(input)
			}
			if(input === 'results' && fns_selector.mode !== 'json') {
				for(let fns_input of fns_selector.form_input) {
					if(fns_input !== 'body' && fns_input !== 'results') {
						let d = fns_selector.data[fns_input]
						for( let node of d.nodes) {
							if(node === '')
								continue
							let el = µ.id('sub_dom').querySelector(`${data.value} ${node}`)
							if(el) {
								d.value = part_selector_get_css_path(el, data.css_class)
								break
							}
						}
					}
				}
			}
			if(input === 'r_dt' && fns_selector.mode !== 'json') {
				part_selector_destroy_regex(input)
				let obj = part_selector_build_fmt_date_obj(data)
				let path = data.value
				let dates = part_selector_get_sub_dom_element(path)
				for(let date of dates) {
					if(date.localName === 'time') {
						let datetime = date.attributes.datetime.value
						if(typeof(datetime) !== 'undefined') {
							add_property(fns.src, 'r_dt_attr', 'datetime')
							break
						}
					}
					let preview = date.textContent
					try {
						mµ.parse_dt_str(
							preview,
							fns_timezone.data.tags_tz.value,
							1,
							obj.fmts,
							fns.month_nb_json)
					} catch(err) {
						part_selector_init_regex(input, data, preview)
						form_addclass(`${input}_reg_input`, 'form_input_required')
						form_addclass(`${input}_reg_input_token`, 'form_input_required')
						form_msg(`${input}_reg_err`, fns_selector.text.MSG_ERROR_DATE_FOUND)
						return
					}
				}
			}
			if(input === 'r_img' && fns_selector.mode !== 'json') {
				let path = data.value
				let img = part_selector_get_sub_dom_element(path)[0]
				if(typeof(img) !== 'undefined') {
					let src = img.src
					let alt = img.alt
					if(src === '') {
						fns_selector.data.r_img_src.skip = false
						fns_selector.data.r_img_src.test_passed = false
						fns_selector.data.r_img_src.value = data.value
					}
					if(alt === '') {
						fns_selector.data.r_img_alt.skip = false
						fns_selector.data.r_img_alt.test_passed = false
						fns_selector.data.r_img_alt.value = data.value
					}
				}
			}
			if(input === 'res_nb') {
				let val = part_selector_sub_dom_get_element_text(input).value
				if(val === '' || val === null) {
					form_input_err(`${input}_btn_attr`,`${input}_err`,
						fns_selector.text.MSG_ERROR_TEXT_CONTENT_EMPTY)
					return
				}
				console.log('TYPEOF', val, typeof(val))
				if(typeof(val) !== 'number') {
					try {
						let reg = new RegExp('\\s*')
						let res = val.match(reg)
						console.log('RES', val, reg, res)
						if(res !== null && res !== []) {
							reg = data.regex
							if(reg === null)
								throw new Error(`No regex found for ${input}`)
							res = µ.regextract(µ.drop_escaped_quotes(reg[0]), val, reg[1])
							console.log('RES 2', val, reg, res)
							if(res === val)
								throw new Error(`Parse don't work for ${input}`)
						}
					} catch ( err ) {
						µ.id(`${input}_btn_reg`).click()
						form_input_err(`${input}_btn_reg`, `${input}_err`,
							fns_selector.text.MSG_ERROR_FAIL_TO_PARSE)
						return
					}
				}
				form_removeclass(`${input}_btn_reg`, 'form_input_required')
				form_msg(`${input}_err`, '')
			}
			if(input === 'body') {
				let path = data.value
				let body = fns.virtual_doc.body
				let el = body.querySelector(path)
				el.value = µ.id('search_terms_input').value
				while(el.localName !== 'form' && el.id !== 'sub_dom')
					el= el.parentNode
				el.addEventListener('submit', (evt) => {
					evt.preventDefault()
					return false
				})
				el.addEventListener('formdata', async (evt) => {
					let url = el.action
					console.log('BEFORE URL', url)
					if(url.startsWith('moz-extension')) {
						let domain_ext = µ.domain_part(url)
						let headline = fns_start.data.headline.value
						url = url.replace(domain_ext, headline)
					}
					console.log('AFTER URL', url)
					let data = evt.formData
					let body = await part_selector_build_post_request(data)
					console.log('BODY', body)
					await part_selector_fetch(url, 'POST', body)
					await part_selector_try_get_virtual_doc()
					body = fns.virtual_doc.body
					body = new DOMParser().parseFromString( body.innerHTML, 'text/html')
					body = body.children[0].children[1]
					part_selector_depth_clean_virtual_doc(body.childNodes)
					part_selector_depth_clean_virtual_doc(body.childNodes)
					part_selector_child_have_text_content(body.childNodes)
					part_selector_set_sub_dom(body)
				})
				el.submit()
			}
			form_display(`${input}_input`, false)
			form_display(`${input}_btn_confirm`, false)
			form_display(`${input}_btn_back`, true)
			fns_selector.mode === 'json' && input === 'results'
				? form_display(`${input}_btn_not_define`, false): null
			typeof(data.attr) !== 'undefined' && fns_selector.mode !== 'json'
				? form_display(`${input}_btn_attr`, true): null
			typeof(data.attr) !== 'undefined' ? form_display(`${input}_btn_reg`, true): null
			form_removeclass(`${input}_input`, 'form_input_required')
			form_msg(`${input}_err`, '')
			part_selector_destroy_regex(input)
			part_selector_destroy_attr(input)
			data.test_passed = true
			part_selector_clear_selection()
			part_selector_update_data_form(input)
			if((fns_selector.old_index === null
				|| fns_selector.old_index === fns_selector.current_index)
				&& form_part_get_data(fns_selector,
					fns_selector.form_input[fns_selector.current_index - 1]).test_passed)
				part_selector_build_input()
			else
				part_selector_input_focus()
			add_property(fns.src, input, data.value)
			form_part_validator(fns_selector, 'finish_selection_part')
		}
		form_display(`${input}_id`, true, 'inline-block')
		fns_selector.current_index += 1
	}

}
/**
 * Update the input data in fns_selector.
 * @param {string} input input name
 */
function part_selector_update_data_form(input) {
	let data = fns_selector.data[input]
	if(input === 'r_url') {
		let path = data.value
		let elements = part_selector_get_sub_dom_element(path)
		console.log(elements)
	}
	if(typeof(data) !== 'undefined' && data.value !== '') {
		let content = part_selector_sub_dom_get_element_text(input)
		if(input === 'results') {
			let path = data.value
			let elements = part_selector_get_sub_dom_element(path)
			form_msg(`${input}_final`, `${elements.length} results in the web page`)
			form_set(`${input}_final`, `${path} (${elements.length})`, 'title')
		} else	{
			form_msg(`${input}_final`, content.value)
			form_set(`${input}_final`, `${content.name}='${content.value}'`, 'title')
		}
		if(typeof(data.regex) !== 'undefined') {
			if(data.type_regex === 'string' && data.regex !== null) {
				let reg = data.regex
				let res = µ.regextract(µ.drop_escaped_quotes(reg[0]), content.value, reg[1])
				form_msg(`${input}_final`, res)
				form_set(`${input}_final`, `${content.name}='${content.value}'`, 'title')
				form_msg(`${input}_final_reg`, `${reg[0]}`)
				form_set(`${input}_final_reg`, `['${reg[0]}', '${reg[1]}']`, 'title')
			} else if(data.type_regex === 'list' && data.regex.length !== 0) {
				let reg = data.regex[0]
				let res = mµ.parse_dt_str(
					content.value,
					fns_timezone.data.tags_tz.value,
					1,
					{
						r_dt_fmt_1: reg
					},
					fns.month_nb_json)
				form_msg(`${input}_final`, res)
				form_set(`${input}_final`, `${content.name}='${content.value}'`, 'title')
				let length = data.regex.length
				if(length > 1) {
					form_msg(`${input}_final_reg`, `(${reg[0]})`)
					form_set(`${input}_final_reg`, `['${reg[0]}', '${reg[1]}']`, 'title')
				} else	{
					form_msg(`${input}_final_reg`, `(${reg[0]})`)
					form_set(`${input}_final_reg`, `['${reg[0]}', '${reg[1]}'] +${length - 1}`, 'title')
				}
			} else {
				form_msg(`${input}_final_reg`, '')
				form_set(`${input}_final_reg`, '', 'title')
			}
		}
		if(typeof(data.attr) !== 'undefined') {
			if(data.attr !== null) {
				if(content.name !== 'textContent') {
					form_msg(`${input}_final_attr`, content.name)
					form_set(`${input}_final_attr`, `${content.name}='${content.value}'`, 'title')
				} else {
					form_msg(`${input}_final`, content.value)
					form_set(`${input}_final`, `${content.name}='${content.value}'`, 'title')
				}
			} else {
				form_msg(`${input}_final_attr`, '')
				form_set(`${input}_final_attr`, '', 'title')
			}
		}
	}
}
/**
 * Get text from an attribute of element.
 * @param {string} input Input name
 * @returns {object} object with name and value of attribute.
 */
function part_selector_sub_dom_get_element_text(input) {
	let data = fns_selector.data[input]
	let path = _.triw(data.value)
	if(fns_selector.mode === 'get' || fns_selector.mode === 'post') {
		let el = µ.id('sub_dom').querySelector(path)
		let attr = data.attr
		attr = _.triw(attr)
		if(attr === null)
			return {name: 'textContent', value: el.textContent}
		else
			return { name: attr, value: el.getAttribute(attr) }
	} else if ( fns_selector.mode === 'json') {
		let el = fns.virtual_doc
		path = path.split('.')
		for(let p of path) {
			if(typeof(el.length) === 'undefined')
				el = el[p]
			else
				el = el[0][p]
		}
		return {name: path, value: el}
	}

}
/**
 * Show in user the tree of this previous selection in the sub dom.
 * @param {string} json_path json path to show.
 * @returns {HTMLElement} last element.
 */
function part_selector_json_show(json_path) {
	let render = µ.id('sub_dom').querySelector('.renderjson')
	let el = render.querySelector('span')
	let path = json_path.split('.')
	let count = 0
	while( count < path.length) {
		let child = null
		for(let c of el.childNodes) {
			if(typeof(c.classList) === 'undefined')
				continue
			if(c.classList.contains('object') && c.classList.length === 1){
				child = c
				break
			} else if(c.classList.contains('array') && c.classList.length === 1 ) {
				child = c
				break
			}
		}
		child.querySelector('.disclosure').click()
		child = child.nextSibling
		if(child.classList[0] === 'object') {
			let key = null
			for(let c of child.childNodes) {
				if(c.textContent === `'${path[count]}'`) {
					key = c
					break
				}
			}
			el = key.nextSibling.nextSibling
		} else if( child.classList[0] === 'array') {
			el = child.childNodes[3]
			continue
		}
		count += 1
	}
	return el
}
/**
 * Give a good focus to input for selector in sub dom.
 */
function part_selector_input_focus() {
	let index = null
	for(let key of fns_selector.form_input) {
		let inp = fns_selector.data[key]
		if(!inp.methode.includes(fns_selector.mode))
			continue
		if(inp.test_passed)
			continue
		if(inp.type === 'optional' && inp.is_passed)
			continue
		index = fns_selector.form_input.indexOf(key)
		break
	}
	if(index !== null) {
		if(fns_selector.old_index === null) {
			fns_selector.old_index = fns_selector.current_index
		} else {
			fns_selector.old_index = fns_selector.old_index === index + 1
				? null: fns_selector.old_index
		}
		fns_selector.current_index = index + 1
		let input = fns_selector.form_input[fns_selector.current_index - 1]
		let data = fns_selector.data[input]
		part_selector_new_sub_dom_listener(input, data)
	}
}
/**
 * Add a event listener for the sub dom.
 * @param {*} input input focused.
 * @param {*} data data of input.
 */
function part_selector_new_sub_dom_listener(input, data) {
	let sub_dom_event = (evt) => {
		let el = evt.target
		part_selector_clear_selection()
		let path
		if(fns_selector.mode === 'get' || fns_selector.mode === 'post') {
			if(!el.classList.contains(data.css_class))
				el.classList.add(data.css_class)
			path = part_selector_get_css_path(el, data.css_class)
		} else if( fns_selector.mode === 'json')
			path = part_selector_get_path_json(el)
		let re = '^.*(#[^#]*)$'
		path = µ.regextract(re, path)
		form_set(`${input}_input`, path, 'value')
	}
	part_selector_remove_sub_dom_listener(sub_dom_event)
	µ.id('sub_dom').onclick = sub_dom_event
}
/**
 * Remove previous event listener
 */
function part_selector_remove_sub_dom_listener(sub_dom_event) {
	µ.id('sub_dom').removeEventListener('click', sub_dom_event)
	/**
				let old_element = µ.id('sub_dom');
				let new_element = old_element.cloneNode(true);
				old_element.parentNode.replaceChild(new_element, old_element);
				if(fns_selector.mode === 'json')
						part_selector_set_sub_dom(fns.virtual_doc)
						*/
}
/**
 * Remove all classes from fns_selector.
 */
function part_selector_clear_selection() {
	for(let cl of fns_selector.class_list) {
		let old_el = document.getElementsByClassName(cl)[0]
		if(old_el !== undefined)
			old_el.classList.remove(cl)
	}
}
/**
 * Find a CSS path for a specified node in sub_dom.
 * @param {HTMLElement} element node to determine a path
 * @param {string} css_class class to ignore
 * @returns {string} CSS path.
 */
function part_selector_get_css_path(elt, css_class){
	let path = []
	while(elt.id !== 'sub_dom') {
		let tmp = ''
		if(elt.id){
			tmp = `#${elt.id}`
		} else if(elt.classList.length !== 0 &&
			elt.classList[0] !== css_class &&
			elt.classList[0] !== fns_selector.sub_dom.class_show_div ) {
			tmp = `.${elt.classList[0]}`
		} else {
			tmp = elt.localName
		}
		path.push(tmp)
		elt = elt.parentNode
	}
	path = path.reverse()
	return path.join(' > ')
}
/**
 * Find a JSON path for a specified node in sub_dom.
 * @param {HTMLElement} el node to determine a path
 * @returns {string} JSON path.
 */
function part_selector_get_path_json(el) {
	let path = []
	path.push(el.textContent.replace(/"/gi, ''))
	while(el.classList[0] !== 'renderjson') {
		el = el.parentNode
		if(el.classList[0] === 'object' || el.classList[0] === 'array') {
			el = el.parentNode
			if(el.parentNode.classList[0] !== 'renderjson') {
				let pre = el
				while( el !== null && el.classList[0] !== 'key' ) {
					pre = el
					el = el.previousSibling
				}
				if(el === null)
					el = pre
			}
		}
		if(el.classList[0] === 'key')
			path.push(el.textContent.replace(/"/gi, ''))
	}
	path = path.reverse()
	return path.join('.')
}
/**
 * Return a list of attributes for an element in sub dom.
 * @param {object} data input data in fns_selector data.
 * @returns {array} list of attributes.
 */
function part_selector_get_content(data) {
	let el = µ.id('sub_dom').querySelector(data.value)
	let black_list = [/^id/, /^class/]
	let attrs = []
	if(el.textContent !== '')
		attrs.push({name: 'textContent', value: el.textContent})
	for(let attr of el.attributes) {
		let ok = true
		for(let reg of black_list) {
			if(attr.localName.match(reg) !== null)
				ok = false
		}
		if(ok)
			attrs.push({name: attr.localName, value: attr.value})
	}
	return attrs
}
/**
 * Init a attr selection part in selector part from dynamic form.
 * @param {array} contents lists of attributes.
 * @param {*} input input in selector sub dom link to these attributes.
 */
function part_selector_init_attr(contents, input) {
	let list = [
		{node: 'div', id:`${input}_attr`},
		{node: 'ul', id: `${input}_attr_list`, parent:0 , classList: ['form_list']},
		{node: 'div', id:`${input}_attr_placholder`, parent: 1,
			classList: ['form_item_placeholder'], textContent: 'Choose a good value'}
	]
	let builder = form_builder(list)
	let parent = µ.id(`${input}_id`)
	parent.appendChild(builder.dom.firstChild)
	let count = 0
	for(let content of contents) {
		let item = [
			{node: 'li', id: `${input}_attr_item_${count}`, classList: ['form_item_list'],
				title: `${content.name}='${content.value}'`},
			{node: 'span', id: `value_attr_item_${count}`, parent: 0, textContent: content.value,
				classList: ['form_item_text']}
		]
		builder = form_builder(item)
		parent = µ.id(`${input}_attr_list`)
		parent.appendChild(builder.dom.firstChild)
		console.log('parent2',parent.outerHTML)
		parent.querySelector(`#${input}_attr_item_${count}`).onclick = () => {
			if(content.name !== 'textContent') {
				let data = fns_selector.data[input]
				data.attr = content.name
				add_property(fns.src, `${input}_attr`, data.attr)
			} else {
				remove_property(fns.src, `${input}_attr`)
			}
			part_selector_destroy_attr(input)
			µ.id(`${input}_btn_confirm`).click()
		}
		count += 1
	}

}
/**
 * Destroy a attr part in selector part from dynamic form.
 * @param {string} input input in selector sub dom.
 */
function part_selector_destroy_attr(input) {
	if(µ.id(`${input}_attr`)) {
		let child = µ.id(`${input}_attr`)
		child.parentNode.removeChild(child)
	}

}
/**
 * Init a regex part in selector part from dynamic form.
 * @param {string} input input in selector sub dom.
 * @param {object} data input data from fns_selector data.
 * @param {string} preview text to parse with regex.
 */
function part_selector_init_regex(input, data, preview) {
	part_selector_destroy_regex(input)
	let template = [
		{node: 'div', id: `${input}_reg`, classList: ['form_list']},
		{node: 'div', id: `${input}_reg_placeholder`, classList: ['form_item_placeholder'],
			textContent: 'Created a regex', parent: 0},
		{node: 'ul', id: `${input}_reg_list`, parent: 0},
		{node: 'p', id: `${input}_reg_preview_text`, parent: 0},
		{node: 'span', textContent: 'Current selected text: ', parent: 3},
		{node: 'span', id: `${input}_reg_preview`, textContent: preview, parent: 3},
		{node: 'div', id: `${input}_reg_input_part`, classList: ['input_container'], parent: 0},
		{node: 'input', id: `${input}_reg_input`, type:'text',
			classList:['form_input_text', 'form_input_text_reg'],
			placeholder:fns_selector.text.MSG_TEXT_REG_PLACEHOLDER, parent: 6},
		{node: 'input', id: `${input}_reg_input_token`, type:'text',
			classList:['form_input_text', 'form_input_text_reg'],
			placeholder:fns_selector.text.MSG_TEXT_TOKEN_PLACEHOLDER, value:'$1', parent: 6},
		{node: 'button', id: `${input}_reg_input_btn_add`, classList: form_btn_classes,
			textContent: 'Add', parent: 6},
		{node: 'button', id: `${input}_reg_input_btn_reset`, classList: form_btn_classes,
			textContent: 'RAZ', parent: 6},
		{node: 'p', id: `${input}_reg_err`, classList:['form_input_error'], parent: 0},
		{node: 'p', id: `${input}_reg_result`, parent: 0}
	]
	let builder = form_builder(template)
	µ.id(`${input}_err`).parentElement.appendChild(builder.dom.firstChild)
	µ.id(`${input}_reg_input_token`).addEventListener('input', () => {
		µ.id(`${input}_reg_input`).dispatchEvent(new Event('input'))
	})
	µ.id(`${input}_reg_input`).addEventListener('input', (evt) => {
		let val = evt.target.value
		val = µ.drop_escaped_quotes(val)
		if(val === '') {
			form_input_err(`${input}_reg_input`, `${input}_reg_err`,
				fns_selector.text.MSG_ERROR_VALUE_EMPTY)
			form_msg(`${input}_reg_result`, '')
			return
		}
		let token = _.triw(µ.id(`${input}_reg_input_token`).value)
		if(token === '') {
			form_input_err(`${input}_reg_input_token`, `${input}_reg_err`,
				fns_selector.text.MSG_ERROR_VALUE_EMPTY)
			form_msg(`${input}_reg_result`, '')
			return
		}
		try {
			new RegExp(val)
		} catch(ex) {
			form_input_err(`${input}_reg_input`, `${input}_reg_err`,
				fns_selector.text.MSG_ERROR_REGEX_NOT_VALID)
			form_msg(`${input}_reg_result`, '')
			return
		}
		let res
		if(input === 'r_dt') {
			try {
				res = mµ.parse_dt_str(
					preview,
					fns_timezone.data.tags_tz.value,
					1,
					{
						r_dt_fmt_1: [
							val,
							token
						]
					},
					fns.month_nb_json)
			} catch(err) {
				form_input_err(`${input}_reg_input`, `${input}_reg_err`,
					fns_selector.text.MSG_ERROR_REGEX_NOT_VALID)
				form_msg(`${input}_reg_result`, '')
				return
			}
		} else {
			res = µ.regextract(val, preview, token)
		}
		if(res === preview || res === '') {
			form_addclass(`${input}_reg_input`, 'form_input_required')
			form_addclass(`${input}_reg_input_token`, 'form_input_required')
			form_msg(`${input}_reg_err`, fns_selector.text.MSG_ERROR_FAIL_TO_PARSE)
			form_msg(`${input}_reg_result`, '')
			return
		}
		form_msg(`${input}_reg_result`, `Result of parse: ${res}`)
		form_removeclass(`${input}_reg_input`, 'form_input_required')
		form_removeclass(`${input}_reg_input_token`, 'form_input_required')
		form_msg(`${input}_reg_err`, '')
	})
	µ.id(`${input}_reg_input_btn_add`).onclick = () => {
		form_removeclass(`${input}_reg_input`, 'form_input_required')
		form_msg(`${input}_reg_err`, '')
		let reg_val = µ.id(`${input}_reg_input`).value
		reg_val = µ.drop_escaped_quotes(reg_val)
		if(reg_val === '') {
			form_input_err(`${input}_reg_input`, `${input}_reg_err`,
				fns_selector.text.MSG_ERROR_VALUE_EMPTY)
			form_msg(`${input}_reg_result`, '')
			return
		}
		let token_val = _.triw(µ.id(`${input}_reg_input_token`).value)
		if(token_val === '') {
			form_input_err(`${input}_reg_input_token`, `${input}_reg_err`,
				fns_selector.text.MSG_ERROR_VALUE_EMPTY)
			form_msg(`${input}_reg_result`, '')
			return
		}
		try {
			new RegExp(reg_val)
		} catch(ex) {
			form_input_err(`${input}_reg_input`, `${input}_reg_err`,
				fns_selector.text.MSG_ERROR_REGEX_NOT_VALID)
			form_msg(`${input}_reg_result`, '')
			return
		}
		let res
		if(input === 'r_dt') {
			try {
				res = mµ.parse_dt_str(
					preview,
					fns_timezone.data.tags_tz.value,
					1,
					{
						r_dt_fmt_1: [
							reg_val,
							token_val
						]
					},
					fns.month_nb_json)
			} catch(err) {
				form_input_err(`${input}_reg_input`, `${input}_reg_err`,	err)
				form_msg(`${input}_reg_result`, '')
				return
			}
		} else {
			res = µ.regextract(reg_val, preview, token_val)
		}
		if(res === preview) {
			form_addclass(`${input}_reg_input`, 'form_input_required')
			form_addclass(`${input}_reg_input_token`, 'form_input_required')
			form_msg(`${input}_reg_err`, fns_selector.text.MSG_ERROR_FAIL_TO_PARSE)
			form_msg(`${input}_reg_result`, '')
			return
		}
		let val = [reg_val, token_val]
		if(data.type_regex === 'string')
			data.regex = val
		else if (data.type_regex === 'list') {
			if(data.index_regex !== -1) {
				data.regex[data.index_regex] = val
				data.index_regex = -1
			} else {
				data.regex.push(val)
			}
		} else
			throw new Error(`You have forget to define 'type_regex' property for '${input}' key in \
				data fns_selector`)
		form_removeclass(`${input}_reg_input`, 'form_input_required')
		form_removeclass(`${input}_reg_input_token`, 'form_input_required')
		form_msg(`${input}_reg_err`, '')
		part_selector_update_item_regex(input, data)
		form_set( `${input}_reg_input`, '', 'value')
		form_set( `${input}_reg_input_token`, '$1', 'value')
		if(data.type_regex === 'string')
			part_selector_destroy_regex(input)
		if(input === 'r_dt') {
			part_selector_set_dt_fmt(data.regex)
		} else {
			add_property(fns.src, `${input}_re`, data.regex)
		}
		part_selector_update_data_form(input)
		µ.id(`${input}_btn_confirm`).click()
	}
	µ.id(`${input}_reg_input_btn_reset`).onclick = () => {
		form_removeclass(`${input}_reg_input`, 'form_input_required')
		form_removeclass(`${input}_reg_input_token`, 'form_input_required')
		form_msg(`${input}_reg_err`, '')
		form_set( `${input}_reg_input`, '', 'value')
		form_set( `${input}_reg_input_token`, '$1', 'value')
		fns_selector.data[input].index_regex = -1
	}
	part_selector_update_item_regex(input, data)
}
/**
 * Destroy a regex part in selector part from dynamic form.
 * @param {*} input input in selector sub dom.
 */
function part_selector_destroy_regex(input) {
	let child = µ.id(`${input}_reg`)
	if(child)
		child.parentNode.removeChild(child)
}
/**
 * Create regex item list.
 * @param {string} input input in selector sub dom.
 * @param {array} reg the list with regex and token defintion.
 * @param {number} number item.
 * @param {HTMLElement} list node to add the build item.
 */
function part_selector_create_item_regex(input, reg, number, list) {
	let template_item = [
		{node: 'li', id: `${input}_reg_item_${number}`, classList: ['form_item_list']},
		{node: 'span', id: `${input}_reg_value_${number}`, textContent: `${reg[0]}`,
			parent: 0, classList: ['form_item_text'], title: `Regex: '${reg[0]}'`,
			Token: `${reg[1]}`},
		{node: 'button', id: `${input}_reg_btn_edit_${number}`, classList: ['form_item_btn'],
			innerHTML: fns.icons.pen, parent:0},
		{node: 'button', id: `${input}_reg_btn_delete_${number}`, classList: ['form_item_btn'],
			innerHTML: fns.icons.cross, parent:0}
	]
	let builder = form_builder(template_item)
	let item = builder.dom.firstChild
	list.appendChild(item)
	µ.id(`${input}_reg_btn_edit_${number}`).onclick = (evt) => {
		let data = fns_selector.data[input]
		let reg = null
		if(data.type_regex === 'string') {
			reg = data.regex
		} else if(data.type_regex === 'list') {
			let index = Number.parseInt(evt.target.id.split('_').pop())
			data.index_regex = index
			reg = data.regex[index]
		}
		if(input === 'r_dt') {
			part_selector_set_dt_fmt(data.regex)
		} else {
			add_property(fns.src, `${input}_re`, data.regex)
		}
		form_set(`${input}_reg_input`,	reg[0], 'value')
		form_set(`${input}_reg_input_token`, reg[1], 'value')
	}
	µ.id(`${input}_reg_btn_delete_${number}`).onclick = (evt) => {
		let data = fns_selector.data[input]
		if(data.type_regex === 'string') {
			data.regex = null
			remove_property(fns.src, `${input}_re`)
		} else if(data.type_regex === 'list') {
			let index = Number.parseInt(evt.target.id.split('_').pop())
			if(index === data.index_regex)
				data.index_regex = -1
			else if (index < data.index_regex)
				data.index_regex -= 1
			data.regex = data.regex.filter( (val, i) => {return i !== index})
			part_selector_set_dt_fmt(data.regex)
		}
		part_selector_update_item_regex(input, data)
	}
}
/**
 * Update a item regex.
 * @param {} input input in selector sub dom.
 * @param {*} data input data from fns_selector data.
 */
function part_selector_update_item_regex(input, data) {
	let list = µ.id(`${input}_reg_list`)
	list.innerHTML = ''
	if(data.regex !== null && data.regex !== []) {
		let count = 0
		if(data.type_regex === 'string') {
			part_selector_create_item_regex(input, data.regex, count, list)
		} else if(data.type_regex === 'list') {
			for(let reg of data.regex) {
				part_selector_create_item_regex(input, reg, count, list)
				count += 1
			}
		} else
			throw new Error(`You have forget to define 'type_regex' property for '${input}' key in \
				data fns_selector`)
	}
}
/**
 * Set all dates formats definitions in new user object.
 * @param {array} regex list of regex definitions.
 */
function part_selector_set_dt_fmt(regex) {
	let count = 1
	fns.src['']
	while( typeof(fns.src[`r_dt_fmt_${count}`]) !== 'undefined' ) {
		remove_property(fns.src, `r_dt_fmt_${count}`)
		count += 1
	}
	count = 1
	for(let dt of regex) {
		add_property(fns.src, `r_dt_fmt_${count}`, dt)
		count += 1
	}
}
/**
 * Build object with all dates formats.
 * @param {object} data input data in fns_selector data.
 * @returns {object} object with the all dates formats and numbers.
 */
function part_selector_build_fmt_date_obj(data) {
	let fmts = {}
	let count = 0
	for(let reg of data.regex) {
		count += 1
		add_property(fmts, `r_dt_fmt_${count}`, reg)
	}
	return {
		fmts: fmts,
		count: count
	}
}
/**
 * Get list of elements with a specified path.
 * @param {string} path path of the element.
 * @returns {array} list of elements in sub dom.
 */
function part_selector_get_sub_dom_element(path) {
	let sub_dom = µ.id('sub_dom')
	if(fns_selector.mode === 'get' || fns_selector.mode === 'post') {
		return sub_dom.querySelectorAll(path)
	} else if(fns_selector.mode === 'json') {
		path = path.split('.')
		let json = fns.virtual_doc
		for(let p of path) {
			json = json[p]
		}
		return json
	}
}
/**
 * Set the new HTML tree in the sub dom.
 * @param {string} src the new HTML tree to set.
 */
function part_selector_set_sub_dom(src) {
	let sub_dom = µ.id('sub_dom')
	sub_dom.innerHTML = ''
	if(fns_selector.mode === 'get' || fns_selector.mode === 'post') {
		sub_dom.innerHTML = src.innerHTML
		let links = sub_dom.querySelectorAll('a')
		for(let link of links) {
			link.setAttribute('disabled', 'true')
			link.disabled = true
			link.onclick = () => false
		}
	} else if(fns_selector.mode === 'json') {
		sub_dom.appendChild( rj.renderjson(src) )
	}
}
/**
 * Init the tags part of dynamic form.
 */
function part_tags_init() {
	let part_tags = µ.id('part_tags_id')
	part_tags.querySelector('legend').onclick = () => form_toggle(part_tags)
	part_tags.style.display = 'block'
	form_toggle(µ.id('part_selector_id'))
	part_tags_event_tech_accuracy()
	part_tags_event_tech_search_system()
	part_tags_event_tech_search_speed()
	part_tags_build_typology('themes', )
	part_tags_build_typology('src_type', )
	part_tags_build_typology('res_type', )
	part_tags_event_btn_save()
}
/**
 * Return a list with all unique values of specfied key from the file source.json.
 * @param {string} key name of property in object from the file source.json.
 * @returns {array} array with all unique values of this property.
 */
function part_tags_init_list(key, ) {
	let items = []
	for(let obj of Object.values(source_objs))
		if(typeof (obj.tags[key]) !== 'undefined')
			if(typeof(obj.tags[key]) === 'string')
				_.array_add_once(items, obj.tags[key])
			else
				items = [...items, ...obj.tags[key].filter((v) => !items.includes(v))]
	return items.sort()
}
/**
 * Return a list of values from input choice in fns_tags.
 * @param {string} key name of input choice in fns_tags.
 * @returns {array} List of values from input choice.
 */
function part_tags_get_list(key) {
	let list = []
	for(let item of fns_tags.choice[key].choiceList.element.childNodes) {
		item = item.dataset.value
		if(!list.includes(item))
			list.push(item)
	}
	return list.sort()
}
/**
 * Build a choice list for the given typology
 */
const typology_select_opt = Object.assign(localisation_select_opt, {removeItemButton: true})
function part_tags_build_typology(name, ) {
	if (!fns_tags.choice[name]) {
		let choices = [default_choice]
		for(let i of part_tags_init_list(name, ))
			choices.push({label: mp_i18n.gettext(i), value: i})
		fns_tags.choice[name] = new Choices(µ.id(`${name}_select`), typology_select_opt)
		fns_tags.choice[name].setChoices(choices, 'value', 'label', false)
		fns_tags.choice[name].passedElement.element.addEventListener('addItem', (evt) => {
			let val = evt.detail.value
			if(val === '')
				return console.error('Empty value')
			if(!part_tags_get_list(name).includes(val)) {
				fns_tags.data[name].value.push(val)
				fns_tags.data[name].test_passed = fns_tags.data[name].value.length !== 0
			}
			add_property(fns.src.tags, name, fns_tags.data[name].value)
			form_part_validator(fns_tags, 'part_tags_btn_next')
		})
		fns_tags.choice[name].passedElement.element.addEventListener('removeItem', (evt) => {
			let val = evt.detail.value
			if(val === '')
				return console.error('Empty value')
			// fns_tags.data[name].value = fns_tags.data[name].value.filter((th) => th !== val)
			_.array_remove_once(fns_tags.data[name].value, val)
			if(fns_tags.data[name].value.length === 0) {
				fns_tags.data[name].test_passed = false
				remove_property(fns.src.tags, name)
			} else {
				add_property(fns.src.tags, name, fns_tags.data[name].value)
			}
			form_part_validator(fns_tags, 'part_tags_btn_next')
		})/*
		µ.id(`${name}_btn_new`).onclick = () => {
			form_display(`${name}_add`, true)
			form_display(`${name}_new`, false)
		}
		µ.id(`${name}_btn_add`).onclick = () => {
			let val = µ.id(`${name}_input_add`).value
			if(val === '')
				return form_input_err(`${name}_input_add`, `${name}_err`, mp_i18n.gettext('Required'))
			val = val.toLowerCase()
			fns_tags.choice[name].setValue([
				{label: val.charAt(0).toUpperCase() + val.substr(1).toLowerCase(), value: val}
			])
			form_addclass(`${name}_input_add`, 'form_input_error')
			form_msg(`${name}_err`, '')
			form_display(`${name}_new`, true)
			form_display(`${name}_add`, false)
		}*/
	}
	/*for (let i of fns_tags.data[name].value)
		if (i)
			fns_tags.choice[name].setChoiceByValue(i)*/
}
/**
 * Create an event for tech accuracy part in tags part.
 */
function accuracy_cb(evt) {
	let val = evt.target.value
	if(evt.target.checked)
		_.array_add_once(fns_tags.data.tech.value, val)
	else
		_.array_remove_once(fns_tags.data.tech.value, val)
	add_property(fns.src.tags, 'tech', fns_tags.data.tech.value)
	fns_tags.data.tech.test_passed = fns_tags.data.tech.value.length !== 0
	form_part_validator(fns_tags, 'part_tags_btn_next')
}
const ACCURACY_CBX = ['many_words', 'one_word', 'exact', 'approx']
function accuracy_exclusive_cb(evt) {
	if(evt.target.checked)
		for (let i of ACCURACY_CBX.filter((t) => t !== evt.target.value))
			if(fns_tags.data.tech.value.includes(i.replace(/_/g, ' ')))
				form_set(`${i}_input`, false, 'checked', 'change')
	accuracy_cb(evt)
}
function part_tags_event_tech_accuracy() {
	µ.id('many_words_input').checked = false
	µ.id('one_word_input').checked = false
	µ.id('exact_input').checked = false
	µ.id('approx_input').checked = false
	//
	µ.id('many_words_input').addEventListener('change', accuracy_cb)
	µ.id('one_word_input').addEventListener('change', accuracy_cb)
	µ.id('exact_input').addEventListener('change', accuracy_exclusive_cb)
	µ.id('approx_input').addEventListener('change', accuracy_exclusive_cb)
}
/**
 * Create an event for tech search system part in tags part.
 */
function part_tags_event_tech_search_system() {
	µ.id('internal_input').checked = false
	µ.id('internal_input').addEventListener('change', (evt) => {
		if(fns_tags.data.tech.value.includes('external search'))
			fns_tags.data.tech.value = fns_tags.data.tech.value.filter(t => t !== 'external search')
		if (evt.target.checked) {
			µ.id('external_input').checked = false
			// internal search is now implicit
			// fns_tags.data.tech.value.push(evt.target.value)
		}
		add_property(fns.src.tags, 'tech', fns_tags.data.tech.value) // update_property
		fns_tags.data.tech.test_passed = fns_tags.data.tech.value.length !== 0
		form_part_validator(fns_tags, 'part_tags_btn_next')
	})
	µ.id('external_input').checked = false
	µ.id('external_input').addEventListener('change', (evt) => {
		if(fns_tags.data.tech.value.includes('internal search'))
			fns_tags.data.tech.value = fns_tags.data.tech.value.filter(t => t !== 'internal search')
		if (evt.target.checked) {
			µ.id('internal_input').checked = false
			fns_tags.data.tech.value.push(evt.target.value)
		}
		add_property(fns.src.tags, 'tech', fns_tags.data.tech.value)
		fns_tags.data.tech.test_passed = fns_tags.data.tech.value.length !== 0
		form_part_validator(fns_tags, 'part_tags_btn_next')
	})
}
/**
 * Create an event for tech search speed part in tags part.
 */
function part_tags_event_tech_search_speed() {
	µ.id('fast_input').addEventListener('change', (evt) => {
		let value = evt.target.value
		if(fns_tags.data.tech.value.includes('slow'))
			fns_tags.data.tech.value = fns_tags.data.tech.value.filter(t => t !== 'slow')
		if (evt.target.checked)
			µ.id('slow_input').checked = false
		fns_tags.data.tech.value.push(value)
		add_property(fns.src.tags, 'tech', fns_tags.data.tech.value)
		fns_tags.data.tech.test_passed = fns_tags.data.tech.value.length !== 0
		form_part_validator(fns_tags, 'part_tags_btn_next')
	})
	µ.id('slow_input').addEventListener('change', (evt) => {
		let value = evt.target.value
		if(fns_tags.data.tech.value.includes('fast'))
			fns_tags.data.tech.value = fns_tags.data.tech.value.filter(t => t !== 'fast')
		if (evt.target.checked)
			µ.id('fast_input').checked = false
		fns_tags.data.tech.value.push(value)
		add_property(fns.src.tags, 'tech', fns_tags.data.tech.value)
		fns_tags.data.tech.test_passed = fns_tags.data.tech.value.length !== 0
		form_part_validator(fns_tags, 'part_tags_btn_next')
	})
}
/**
 * Create an event for button save part in tags part.
 */
async function save_custom_src() {
	// for(let i of ['start_form', 'timezone', 'selector', 'tags'])
	// 	form_toggle(µ.id(`part_${i}_id`))
	form_toggle(µ.id('part_tags_id'))
	add_property(fns.src, 'tags', fns.src.tags)
	console.log('created src', fns.src)
	let old = await mµ.get_custom_src()
	let obj = {}
	if (typeof(old) === 'object' && Object.keys(old) > 0)
		obj = JSON.parse(old)
	obj[new URL(fns_start.data.src_url.value).origin] = sµ.reformat_source(
		fns.src, sµ.SRC_DEF_TESTS.format_order)
	await mµ.to_storage('custom_src', obj)
	mµ.update_need_reload_src()
}
function part_tags_event_btn_save() {
	µ.id('part_tags_btn_save_id').onclick = async () => {
		await save_custom_src()
		window.location.reload()
	}
	µ.id('part_tags_btn_edit_id').onclick = async () => {
		await save_custom_src()
		window.location = '/html/custom_source.html'
	}
	µ.id('part_tags_btn_next').onclick = () => {
		µ.id('contributional_footer').style.display = 'block'
		µ.id('part_tags_btn_save_id').style.display = 'inline-block'
		µ.id('part_tags_btn_edit_id').style.display = 'inline-block'
	}
}
/**
 * Bind all tips
 */
function bind_all_tips() {
	for (let a of Array.from(µ.$$('.form_input_tips')).concat(Array.from(µ.$$('.tips')))) {
		let prefix = a.id.split('_tips')[0]
		let btn_show = µ.id(`${prefix}_display_tips`)
		let btn_hide = µ.id(`${prefix}_hide_tips`)
		btn_show.onclick = () => form_tips(btn_show.id, a.id)
		btn_hide.onclick = () => form_tips(btn_show.id, a.id)
	}
}
function bind_all_clearers() {
	for (let a of µ.$$('.input_clearer'))
		a.onclick = () => {
			let b = µ.id(`${a.id.split('_clearer')[0]}_input`)
			b.value = ''
			b.dispatchEvent(new Event('input'))
		}
}
/**
 * End of definitions
 */
async function init () {
	const userLang = await mµ.get_wanted_locale()
	mp_i18n = await g.gettext_html_auto(userLang)
	LANG_NAME = new Intl.DisplayNames([userLang], {type: 'language'})
	// const COUNTRY_NAME = new Intl.DisplayNames([userLang], {type: 'region'})
	source_objs = await mµ.get_built_src()
	const source_keys = Object.keys(source_objs)
	default_choice = {label: mp_i18n.gettext('Click and select'), value: '', placeholder:true}
	if (QUERYSTRING.get('xgettext'))
		await g.xgettext_html()
	const name_src = QUERYSTRING.get('src')
	if(name_src && name_src !== '')
		load_existing_src (name_src, )
	reset_fns()
	if(default_val.key !== ''){
		fns.src = source_objs[default_val.key]
		fns.src.tags = source_objs[default_val.key].tags
	}
	µ.id('mp_version').textContent = 'v' + (await xµ.get_manifest()).version
	µ.id('mp_src_total').textContent = source_keys.length
	µ.id('mp_src_countries').textContent = Object.values(source_objs).reduce((total, elt) => {
		return total.add(elt.tags.country)
	}, new Set()).size
	µ.id('mp_src_langs').textContent = Object.values(source_objs).reduce((total, elt) => {
		return total.add(elt.tags.lang)
	}, new Set()).size
	bind_all_tips()
	bind_all_clearers()
	part_start_form_event(source_keys)
	// form_toggle(µ.id('part_start_form_id'))
	// part_timezone_init()
	// form_toggle(µ.id('part_timezone_id'))
	// part_selector_init()
	// part_tags_init()
	// form_toggle(µ.id('part_tags_id'))
	document.body.classList.add('javascript_loaded')
}
init()
