// SPDX-FileName: ./page_testing.js
// SPDX-FileCopyrightText: 2022 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

function Exc(msg) {
	return `[${new URL(window.location).pathname}] ${msg}\n${new Error().stack}`
}
window.are_imgs_loaded = async function are_imgs_loaded() {
	try {
		const imgs = document.querySelectorAll('img')
		let src
		for (const i of imgs) {
			src = i.attributes.src.value
			if (!src)
				throw `src= not defined for id=${i.id} class=${i.classList}`
			if (!i.attributes.alt.value)
				throw `alt= not defined for id=${i.id} class=${i.classList}`
			if (i.checkVisibility() && src && src !== '.') {
				if (i.naturalWidth === 0)
					throw `${src} -> naturalWidth === 0`
				try {
					await i.decode()
				} catch (exc) {
					throw `${src} -> can't .decode() ${i}`
				}
			}
		}
	} catch (exc) {
		throw Exc(exc)
	}
}
window.are_internal_links_broken = async () => {
	try {
		const links = document.querySelectorAll('a')
		let href, raw
		for (const a of links) {
			href = a.attributes.href.value
			// console.log('href', href)
			if (a.checkVisibility() && href && href.startsWith('moz-extension:')) {
				raw = await fetch(href)  // fails because of CORS
				if (!raw.ok)
					throw `${href} -> not ok ${raw.reason}`
				if (raw.status !== 200)
					throw `${href} -> not 200 but ${raw.status}`
			}
		}
	} catch (exc) {
		throw Exc(exc)
	}
}

async function selenium_answered(selenium_read_var, test_fn_hook) {
	try {
		window[selenium_read_var] = false
		await test_fn_hook()
	} catch (exc) {
		console.warn(exc)
		return
	}
	return window[selenium_read_var] = true
}
window.dummy_test = (selenium_read_var) => selenium_answered(selenium_read_var, () => true)
window.test_img_loading = (a) => selenium_answered(a, window.are_imgs_loaded)
// test_img_loading('oaue')
// test_links_loading()
